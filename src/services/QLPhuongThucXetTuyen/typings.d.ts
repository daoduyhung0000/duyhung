import { stringOrDate } from 'react-big-calendar';

declare module QLPhuongThucXetTuyen {
  export interface Data {
    limit: number;
    offset: number;
    page: number;
    result: Record[];
    total: number;
    statusCode: number;
  }
  export interface Record {
    createdAt: stringOrDate;
    hinhThucDaoTao: HinhThucDaoTao[];
    maPhuongThuc: string;
    tenPhuongThuc: string;
    updatedAt: stringOrDate;
    __v: number;
    _id: string;
  }
  export interface HinhThucDaoTao {
    createdAt: stringOrDate;
    kyHieu: string;
    ma: string;
    ten: string;
    updatedAt: stringOrDate;
    __v: number;
    _id: string;
  }
}
