import { ip3 } from '@/utils/ip';
import axios from 'axios';

const getAllHinhThucDaoTao = async () => {
  return await axios.get(`${ip3}/hinh-thuc-dao-tao/all`);
};

export { getAllHinhThucDaoTao };
