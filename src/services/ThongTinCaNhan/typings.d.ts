import { EGender, Vocative } from '@/utils/constants';

declare module ThongTinCaNhan {
  export interface Name {
    title: Vocative.MALE | Vocative.FEMALE;
    first: string;
    last: string;
  }
  export interface Coordinates {
    latitude: number;
    longitude: number;
  }
  export interface TimeZone {
    offset: string;
    description: string;
  }
  export interface Location {
    street: string;
    city: string;
    state: string;
    postcode: number;
    coordinates: Coordinates;
    timezone: TimeZone;
  }
  export interface Login {
    uuid: number;
    username: string;
    password: string;
    salt: string;
    md5: string;
    sha1: string;
    sha256: string;
  }
  export interface DobAndRegistered {
    date: string;
    age: number;
  }
  export interface Id {
    name: string;
    value: number;
  }
  export interface Picture {
    large: string;
    medium: string;
    thumbnail: string;
  }
  export interface Info {
    seed: string;
    results: number;
    page: number;
    version: string;
  }
  export interface Record {
    gender: EGender;
    name: Name;
    location: Location;
    email: string;
    login: Login;
    dob: DobAndRegistered;
    registered: DobAndRegistered;
    phone: number;
    cell: string;
    id: Id;
    picture: Picture;
    nat: string;
    info: Info;
  }
}
