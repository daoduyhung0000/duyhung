declare module QuanLyCoSoDaoTao {
  export interface Data {
    limit: number;
    offset: number;
    page: number;
    result: Record[];
    total: number;
  }
  export interface Record {
    createAt: string;
    kyHieu: string;
    ten: string;
    tenVietTat: string;
    updateAt: string;
    __v: number;
    _id: string;
  }
}
