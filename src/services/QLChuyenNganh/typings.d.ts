import { stringOrDate } from 'react-big-calendar';

declare module QlChuyenNganh {
  export interface Record {
    _id: number;
    ten: string;
    ma: number;
    kyHieu: string;
    createdAt: stringOrDate;
    updateAt: stringOrDate;
    __v: number;
  }
}
