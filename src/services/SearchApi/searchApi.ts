import axios from "axios"

const getByName = async (name: string) =>{
    const config = {
        params: {name: name}
    }
    return await axios.get(`https://jsonplaceholder.typicode.com/comments`, config)
}

export {getByName}