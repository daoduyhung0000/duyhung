import { ip3 } from '@/utils/ip';
import axios from 'axios';

const getAllCoSoDaoTao = async () => {
  return await axios.get(`${ip3}/co-so-dao-tao/all`);
};
const getAllNganhChuyenNganh = async () => {
  return await axios.get(`${ip3}/nganh-chuyen-nganh/all`);
};
const getAllHinhThucDaoTao = async () => {
  return await axios.get(`${ip3}/hinh-thuc-dao-tao/all`);
};
const getAllNamTuyenSinh = async () => {
  return await axios.get(`${ip3}/nam-tuyen-sinh/all`);
};
const getAllDoiTuongTuyenSinh = async () => {
  return await axios.get(`${ip3}/doi-tuong-tuyen-sinh/all`);
};
const getPayMent = async () => {
  return await axios.get(`${ip3}/payment/product-external/pageable`, {
    params: {
      page: 1,
      limit: 100,
      cond: { 'metaData.loai': 'Tuyển sinh', 'metaData.hinhThuc': 'Xét tuyển', active: true },
    },
  });
};
export {
  getAllCoSoDaoTao,
  getAllNganhChuyenNganh,
  getAllHinhThucDaoTao,
  getAllNamTuyenSinh,
  getAllDoiTuongTuyenSinh,
  getPayMent,
};
