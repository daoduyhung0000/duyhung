declare module QLDotTuyenSinh {
  export interface Record {
    cauHinhPhuongThuc: object;
    choPhepDangKyKhacCoSo: boolean;
    choPhepGiaLapTheoCoSo: boolean;
    choPhepHK1HoacCaNamLop12: boolean;
    choPhepThiSinhMoKhoa: boolean;
    createdAt: string;
    danhSachCoSoDaoTao: any[];
    danhSachDoiTuongTuyenSinh: DanhSachDoiTuongTuyenSinh[];
    danhSachGiayToXacNhanNhapHoc: DanhSachGiayToXacNhanNhapHoc;
    danhSachHuongDanSuDung: any[];
    danhSachNganhTuyenSinh: DanhSachNganhTuyenSinh[];
    danhSachPhuongThucTuyenSinh: DanhSachPhuongThucTuyenSinh[];
    danhSachThongTinKhaiXacNhan: DanhSachThongTinKhaiXacNhan[];
    danhSachToHopLuaChon: any[];
    duocPhepCapNhatSauHanNop: boolean;
    gioiHanDoiTuong: boolean;
    hinhThucDaoTao: HinhThucDaoTao;
    id: string;
    loaiDot: string;
    maDotTuyenSinh: number;
    moTa: string;
    namTuyenSinh: number;
    sendMailKhoa: boolean;
    sendMailKhongTiepNhan: boolean;
    sendMailThanhToan: boolean;
    sendMailTiepNhan: boolean;
    soLuongDangKy: number;
    suDungToHopMongMuon: boolean;
    tenDotTuyenSinh: string;
    thoiGianBatDauXacNhanNhapHoc: string;
    thoiGianCongBoKetQua: string;
    thoiGianKetThucNopHoSo: string;
    thoiGianKetThucXacNhanNhapHoc: string;
    thoiGianMoDangKy: string;
    thoiGianKetThucCapNhatSauHanNop: string;
    thongTinGiayToNopHoSo: ThongTinGiayToNopHoSo[];
    thongTinGiayToNopOnline: ThongTinGiayToNopHoSo[];
    updatedAt: string;
    urlRedirectLoaiDot: string;
    idLePhi: string;
    hinhThucThanhToan: string;
    yeuCauTraPhi: boolean;
    __v: number;
    _id: string;
    soLuongNguyenVongToiDa: number;
    huongDanThanhToan: string;
    mauPhieuDangKy: MauPhieuDangKy;
  }
  export interface DanhSachGiayToXacNhanNhapHoc {
    maGiayTo: string;
    required: boolean;
    textHuongDan: string;
    tieuDe: string;
    urlGiayTo: any[];
    urlHuongDan: string[];
    _id: string;
  }
  export interface DanhSachPhuongThucTuyenSinh {
    createdAt: string;
    hinhThucDaoTao: string;
    maPhuongThuc: string;
    tenPhuongThuc: string;
    updatedAt: string;
    __v: number;
    _id: string;
  }
  export interface DanhSachThongTinKhaiXacNhan {
    maThongTin: string;
    required: boolean;
    textHuongDan: string;
    tieuDe: string;
    urlHuongDan: any[];
    urlGiayTo: any[];
    _id: string;
  }
  export interface HinhThucDaoTao {
    createdAt: string;
    kyHieu: string;
    ma: string;
    ten: string;
    updatedAt: string;
    __v: number;
    _id: string;
  }
  export interface DanhSachDoiTuongTuyenSinh {
    cauHinhDoiTuong: {
      cauHinh: {};
      danhSach: {
        thongTinDuLieuXetTuyenThang: {
          cauHinh: {};
          danhSach: {
            tenDoiTuongDuLieuXetTuyenThang: { required: boolean };
            urlDoiTuongDuLieuXetTuyenThang: { required: boolean; maxLength: number; hint: string };
          };
        };
      };
    };
    cauHinhQuyDoi: any[];
    cauHinhValidateTheoNganhToHopCoSo: any[];
    congThucPreviewQuyDoi: {};
    hienThiDiemQuyDoi: boolean;
    hienThiPreviewDiemQuyDoi: boolean;
    id: string;
    maDoiTuong: string;
    phuongThucTuyenSinh: string;
    thongTinDoiTuong: ThongTinDoiTuong;
    yeuCauChonToHop: boolean;
    _id: string;
  }
  export interface ThongTinDoiTuong {
    createdAt: string;
    hinhThucDaoTao: HinhThucDaoTao;
    maDoiTuong: string;
    tenDoiTuong: string;
    updatedAt: string;
    __v: string;
    _id: string;
  }
  export interface DanhSachNganhTuyenSinh {
    danhSachCoSoDaoTao: CosoDaoTao[];
    danhSachToHop: string[];
    nganh: NganhChuyenNganh;
    _id: string;
  }
  export interface ThongTinGiayToNopHoSo {
    ten: string;
    soLuong: number;
    required: boolean;
    requiredOnline: boolean;
    ghiChu: string;
    soLuongNop: number;
    ghiChuNop: string;
    urlGiayToNop: string[];
    soLuongTiepNhan: number;
    ghiChuTiepNhan: string;
    maGiayTo: string;
    thoiGianNop: string;
    textHuongDan: string;
    urlHuongDan: string[];
  }
  export interface MauPhieuDangKy {
    url: string;
  }
  export interface DanhSachHuongDanSuDung {
    tenHuongDan: string;
    tepDinhKem: string;
  }
}

declare module CosoDaoTao {
  export interface Record {
    createdAt: string;
    kyHieu: string;
    ten: string;
    tenVietTat: string;
    updatedAt: string;
    __v: number;
    _id: string;
  }
}

declare module HinhThucDaoTao {
  export interface Record {
    createdAt: string;
    kyHieu: string;
    ma: string;
    ten: string;
    updatedAt: string;
    __v: number;
    _id: string;
  }
}

declare module NamTuyenSinh {
  export interface Record {
    createdAt: string;
    danhSachPhuongThuc: DanhSachPhuongThuc;
    hinhThucXetTuyen: string;
    hinhThucDaoTao: HinhThucDaoTao;
    moTa: string;
    nam: number;
    noiDung: string;
    updatedAt: string;
    urlAnhMoTa: string;
    __v: number;
    _id: string;
  }
  export interface DanhSachPhuongThuc {
    phuongThucTuyenSinh: PhuongThucTuyenSinh;
    _id: string;
  }
  export interface PhuongThucTuyenSinh {
    createdAt: string;
    hinhThucDaoTao: string;
    maPhuongThuc: string;
    tenPhuongThuc: string;
    updatedAt: string;
    __v: number;
    _id: string;
  }
}

declare module NganhChuyenNganh {
  export interface Record {
    createdAt?: string;
    kyHieu?: string;
    ma?: string;
    ten?: string;
    updatedAt?: string;
    __v?: number;
    _id?: string;
  }
}

declare module DoiTuongTuyenSinh {
  export interface Record {
    createdAt: string;
    maDoiTuong: string;
    tenDoiTuong: string;
    updatedAt: string;
    __v: number;
    _id: string;
    hinhThucDaoTao: HinhThucDaoTao;
  }
}
