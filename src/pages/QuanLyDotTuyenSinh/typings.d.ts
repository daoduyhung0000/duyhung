declare module TableProps {
  export interface Props {
    modelName: any;
    Form?: React.FC;
    formType?: 'Modal' | 'Drawer';
    columns: IColumn<any>[];
    title?: React.ReactNode;
    widthDrawer?: string | number;
    getData: function;
    dependencies?: any[];
    loading: boolean;
    params?: any;
    children?: React.ReactNode;
    border?: boolean;
    hascreate?: boolean;
    dataState?: string;
    otherProps?: TableProps<any>;
    maskCloseableForm?: boolean;
    noCleanUp?: boolean;
    hideCard?: boolean;
  }
}

declare module ChildProps {
  export interface Props{
    title: string, 
    nameForm?: string, 
    columns?: any, 
    Form?: React.FC, 
    showForm: boolean, 
    setShowForm: function, 
    data: any, 
    setIsEdit: function,
    isEdit: boolean,
    initialValue: any,
    setInitialValue: function
  }
}

type InitialValue<T> = {
  object?: T,
  index?: number
}

type PhuongThucTuyenSinhValue = {
  value: any,
  key: string, 
  name: string
}