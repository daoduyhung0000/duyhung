import TinyEditor from '@/components/TinyEditor/Tiny';
import {
  Button,
  Checkbox,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  Space,
} from 'antd';
import _ from 'lodash';
import { useEffect, useMemo, useState } from 'react';
import { useModel } from 'umi';
import rules from '@/utils/rules';
import moment from 'moment';
import DanhSachDoiTuongTable from './table/DanhSachDoiTuongTable';
import DanhSachNganhTuyenSinhTable from './table/DanhSachNganhTuyenSinhTable';
import DanhSachGiayToNopHoSoTable from './table/DanhSachGiayToNopHoSo';
import DanhSachGiayToNopHoSoOnlineTable from './table/DanhSachGiayToNopOnline';
import DanhSachGiayToXacNhanTable from './table/DanhSachGiayToXacNhanNhapHoc';
import DanhSachThongTinKhaiXacNhanTable from './table/DanhSachThongTinKhaiXacNhanNhapHocTable';
import DanhSachHuongDanSuDungTable from './table/DanhSachHuongDanSuDungTable';
import { EHinhThucThanhToan, MapKeyHinhThucThanhToan } from '@/utils/constants';
import Upload from '@/components/Upload/UploadMultiFile';
import { renderFileList, uploadMultiFile } from '@/utils/utils';

const MainForm: React.FC = () => {
  const [needPay, setNeedPay] = useState<boolean>(false);
  const [dangKiCoSo, setDangKiCoSo] = useState<boolean>(false);
  const [capNhatHoSo, setCapNhatHoSo] = useState<boolean>(false);
  const [amount, setAmount] = useState<number[]>([]);
  const [amountInitial, setAmountInitial] = useState<number>();
  const [donVi, setDonVi] = useState<string>();
  const [hinhThucThanhToan, setHinhThucThanhToan] = useState<string>();

  const { Option } = Select;
  const [form] = Form.useForm();

  const {
    hinhThucDaoTaoDanhSach,
    namTuyenSinhDanhSach,
    hinhThucDaoTaoValue,
    doiTuongTuyenSinhDanhSach,
    namValue,
    setHinhThucDaoTaoValue,
    setNamValue,
    phuongThucTuyenSinhValue,
    setPhuongThucTuyenSinhValue,
    danhSachDoiTuong,
    setDanhSachDoiTuong,
    danhSachNganhTuyenSinh,
    setDanhSachNganhTuyenSinh,
    danhSachGiayToNopHoSo,
    setDanhSachGiayToNopHoSo,
    danhSachGiayToXacNhan,
    setDanhSachGiayToXacNhan,
    danhSachGiayToNopHoSoOnline,
    setDanhSachGiayToNopHoSoOnline,
    danhSachThongTinKhaiXacNhan,
    setDanhSachThongTinKhaiXacNhan,
    danhSachHuongDanSuDung,
    setDanhSachHuongDanSuDung,
    payMentDanhSach,
    postModel,
    setVisibleForm,
    edit,
    initialValue,
    putModel,
  } = useModel('qldottuyensinh');
  useEffect(() => {
    if (edit) {
      setHinhThucDaoTaoValue(initialValue?.hinhThucDaoTao?._id);
      namTuyenSinhDanhSach.forEach((data: NamTuyenSinh.Record) => {
        if (data?.nam == initialValue?.namTuyenSinh) {
          setNamValue(data?._id);
        }
      });
      setDanhSachNganhTuyenSinh(changeData('NganhTuyenSinh', initialValue?.danhSachNganhTuyenSinh));
      setDanhSachGiayToNopHoSo(changeData('GiayToNopHoSo', initialValue?.thongTinGiayToNopHoSo));
      setDanhSachGiayToNopHoSoOnline(
        changeData('GiayToNopHoSoOnline', initialValue?.thongTinGiayToNopOnline),
      );
      setDanhSachGiayToXacNhan(
        changeData('GiayToXacNhanNhapHoc', initialValue?.danhSachGiayToXacNhanNhapHoc),
      );
      setDanhSachThongTinKhaiXacNhan(
        changeData('KhaiXacNhan', initialValue?.danhSachThongTinKhaiXacNhan),
      );
      setDanhSachHuongDanSuDung(changeData('HuongDan', initialValue?.danhSachHuongDanSuDung));
      setNeedPay(initialValue.yeuCauTraPhi);
      setDangKiCoSo(initialValue?.choPhepDangKyKhacCoSo);
      _.map(payMentDanhSach, (data) => {
        const value = _.find(
          data?.prices,
          (prices) => prices.active === true && prices._id === initialValue?.idLePhi,
        );
        if (value) {
          setAmount([].concat(value?.unitAmount));
          setDonVi(data?.unitLabel);
          setAmountInitial(value?.unitAmount);
        }
      });
      setHinhThucThanhToan(initialValue?.hinhThucThanhToan);
      setCapNhatHoSo(initialValue?.duocPhepCapNhatSauHanNop);
    } else {
      setPhuongThucTuyenSinhValue([]);
      setNamValue('');
      setHinhThucDaoTaoValue('');
      setDanhSachDoiTuong([]);
      setDanhSachNganhTuyenSinh([]);
      setDanhSachGiayToNopHoSo([]);
      setDanhSachGiayToNopHoSoOnline([]);
      setDanhSachGiayToXacNhan([]);
      setDanhSachThongTinKhaiXacNhan([]);
      setDanhSachHuongDanSuDung([]);
      setNeedPay(false);
      setDangKiCoSo(false);
      setAmount([]);
      setDonVi('');
      setHinhThucThanhToan('');
      setCapNhatHoSo(false);
      setAmountInitial(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [edit, initialValue]);
  useEffect(() => {
    if (edit) {
      function changePhuongThucTuyenSinhValue() {
        const danhSach = _.filter(namTuyenSinhDanhSach, (data: NamTuyenSinh.Record) => {
          return data?._id == namValue;
        }).map((data: NamTuyenSinh.Record) => {
          return data?.danhSachPhuongThuc;
        });
        const arr = _.flatten(danhSach).filter((data) => {
          return (
            _.map(initialValue?.danhSachPhuongThucTuyenSinh, (val) => val._id).indexOf(
              data?.phuongThucTuyenSinh?._id,
            ) !== -1
          );
        });
        return arr.map((data: NamTuyenSinh.DanhSachPhuongThuc) => {
          return {
            name: data?.phuongThucTuyenSinh?.tenPhuongThuc,
            key: data?._id,
            value: data?.phuongThucTuyenSinh?._id,
          };
        });
      }
      setPhuongThucTuyenSinhValue(changePhuongThucTuyenSinhValue());
      setDanhSachDoiTuong(changeData('DoiTuong', initialValue?.danhSachDoiTuongTuyenSinh));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [namValue]);

  useEffect(() => {
    form.resetFields(['donViTinh', 'mucLePhi']);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [amount, needPay, donVi]);

  function showDanhSachPhuongThuc() {
    if(namValue && hinhThucDaoTaoValue){
      const danhSach = _.filter(namTuyenSinhDanhSach, (data: NamTuyenSinh.Record) => {
        return data?._id == namValue;
      }).map((data: NamTuyenSinh.Record) => {
        return data?.danhSachPhuongThuc;
      });
      return _.flatten(danhSach).map((data: NamTuyenSinh.DanhSachPhuongThuc) => {
        return (
          <Option
            key={data?._id}
            value={data?.phuongThucTuyenSinh?._id}
            name={data?.phuongThucTuyenSinh?.tenPhuongThuc}
          >
            {data?.phuongThucTuyenSinh?.tenPhuongThuc}
          </Option>
        );
      });
    }else{
      return []
    }
    
  }
  function changeData(type: string, data: any) {
    console.log(1)
    switch (type) {
      case 'DoiTuong':
        return _.map(data, (val) => {
          const mapDoiTuong = _.find(doiTuongTuyenSinhDanhSach, (doiTuongObj) => {
            return doiTuongObj?._id == val?.thongTinDoiTuong?._id;
          });
          return {
            cauHinhDoiTuong: val?.cauHinhDoiTuong,
            cauHinhQuyDoi: val?.cauHinhQuyDoi,
            cauHinhValidateTheoNganhToHopCoSo: val?.cauHinhValidateTheoNganhToHopCoSo,
            congThucPreviewQuyDoi: val?.congThucPreviewQuyDoi,
            hienThiDiemQuyDoi: val?.hienThiDiemQuyDoi,
            hienThiPreviewDiemQuyDoi: val?.hienThiPreviewDiemQuyDoi,
            maDoiTuong: val?.maDoiTuong,
            phuongThucTuyenSinh: val?.phuongThucTuyenSinh,
            yeuCauLuaChonToHop: val?.yeuCauLuaChonToHop,
            thongTinDoiTuong: {
              hinhThucDaoTao: mapDoiTuong?.hinhThucDaoTao,
              maDoiTuong: val?.thongTinDoiTuong?.maDoiTuong,
              moTa: val?.thongTinDoiTuong?.moTa || '',
              tenDoiTuong: val?.thongTinDoiTuong?.tenDoiTuong,
            },
          };
        });
        break;
      case 'NganhTuyenSinh':
        return data?.map((value: QLDotTuyenSinh.DanhSachNganhTuyenSinh) => {
          return {
            danhSachCoSoDaoTao: value?.danhSachCoSoDaoTao.map((objValue) => objValue?._id),
            danhSachToHop: value?.danhSachToHop,
            nganh: value?.nganh?._id,
          };
        });
        break;
      case 'GiayToNopHoSo':
        return data?.map((value: QLDotTuyenSinh.ThongTinGiayToNopHoSo) => {
          return {
            ghiChu: value?.ghiChu || '',
            maGiayTo: value?.maGiayTo,
            required: value?.required,
            soLuong: value?.soLuong,
            ten: value?.ten,
            textHuongDan: value?.textHuongDan || '',
            urlHuongDan: value?.urlHuongDan,
            urlGiayToNop: value?.urlGiayToNop || [],
          };
        });
      case 'GiayToNopHoSoOnline':
        return data?.map((value: QLDotTuyenSinh.ThongTinGiayToNopHoSo) => {
          return {
            ghiChu: value?.ghiChu || '',
            maGiayTo: value?.maGiayTo,
            required: value?.required,
            soLuong: value?.soLuong,
            ten: value?.ten,
            textHuongDan: value?.textHuongDan || '',
            urlHuongDan: value?.urlHuongDan,
            urlGiayToNop: value?.urlGiayToNop || [],
          };
        });
      case 'GiayToXacNhanNhapHoc':
        return data?.map((value: QLDotTuyenSinh.DanhSachGiayToXacNhanNhapHoc) => {
          return {
            maGiayTo: value?.maGiayTo,
            required: value?.required,
            tieuDe: value?.tieuDe,
            textHuongDan: value?.textHuongDan || '',
            urlHuongDan: value?.urlHuongDan || [],
            urlGiayTo: value?.urlGiayTo || [],
          };
        });
      case 'KhaiXacNhan':
        return data?.map((value: QLDotTuyenSinh.DanhSachThongTinKhaiXacNhan) => {
          return {
            maThongTin: value?.maThongTin,
            required: value?.required,
            tieuDe: value?.tieuDe,
            textHuongDan: value?.textHuongDan || '',
            urlHuongDan: value?.urlHuongDan || [],
            urlGiayTo: value?.urlGiayTo || [],
          };
        });
      case 'HuongDan':
        return data?.map((value: QLDotTuyenSinh.DanhSachHuongDanSuDung) => {
          return {
            tenHuongDan: value?.tenHuongDan,
            tepDinhKem: value?.tepDinhKem || '',
          };
        });
        defualt: break;
    }
  }
  return (
    <>
      <Row
        style={{
          padding: '1.5rem',
        }}
      >
        <Col
          span={24}
          style={{
            fontSize: '1rem',
            borderBottom: '1px solid whitesmoke',
            paddingBottom: '1.5rem',
          }}
        >
          Thêm mới đợt tuyển sinh
        </Col>
        <Form
          onFinish={async (values) => {
            const urlMauPhieu = await uploadMultiFile(
              values?.mauPhieuDangKy?.fileList ?? [],
              true,
              false,
            );
            values.mauPhieuDangKy = urlMauPhieu?.[0]?.file?.id;
            values.moTa = values.moTa.text;
            if (values.huongDanThanhToan) values.huongDanThanhToan = values.huongDanThanhToan.text;
            values.cauHinhPhuongThuc = {};
            values.thoiGianMoDangKy = moment(values.thoiGianMoDangKy).toISOString(false);
            values.thoiGianBatDauXacNhanNhapHoc = moment(
              values.thoiGianBatDauXacNhanNhapHoc,
            ).toISOString(false);
            values.thoiGianCongBoKetQua = moment(values.thoiGianCongBoKetQua).toISOString(false);
            values.thoiGianKetThucNopHoSo = moment(values.thoiGianKetThucNopHoSo).toISOString(
              false,
            );
            values.thoiGianKetThucXacNhanNhapHoc = moment(
              values.thoiGianKetThucXacNhanNhapHoc,
            ).toISOString(false);
            values.thoiGianKetThucCapNhatSauHanNop = moment(
              values.thoiGianKetThucCapNhatSauHanNop,
            ).toISOString(false);

            values.danhSachDoiTuongTuyenSinh = danhSachDoiTuong;
            values.danhSachNganhTuyenSinh = danhSachNganhTuyenSinh;
            values.thongTinGiayToNopHoSo = danhSachGiayToNopHoSo;
            values.thongTinGiayToNopOnline = danhSachGiayToNopHoSoOnline;
            values.danhSachGiayToXacNhanNhapHoc = danhSachGiayToXacNhan;
            values.danhSachThongTinKhaiXacNhan = danhSachThongTinKhaiXacNhan;
            values.danhSachHuongDanSuDung = danhSachHuongDanSuDung;
            if (edit) {
              putModel(initialValue?._id, values);
            } else {
              postModel(values);
            }
          }}
          layout="vertical"
          style={{
            marginTop: '1.5rem',
            width: '100%',
          }}
          form={form}
        >
          <Form.Item
            label="Tên đợt tuyển sinh"
            name="tenDotTuyenSinh"
            rules={[{ required: true, message: 'Bắt buộc' }]}
            initialValue={edit ? initialValue?.tenDotTuyenSinh : ''}
          >
            <Input placeholder="Tên đợt tuyển sinh" />
          </Form.Item>
          <Row gutter={[16, 16]}>
            <Col span={6}>
              <Form.Item
                label="Hình thức đào tạo"
                name="hinhThucDaoTao"
                rules={[{ required: true, message: 'Bắt buộc' }]}
                initialValue={edit ? initialValue?.hinhThucDaoTao?._id : []}
              >
                <Select
                  placeholder="Hình thức đào tạo"
                  onChange={(value) => {
                    setHinhThucDaoTaoValue(value);
                    form.resetFields(['namTuyenSinh', 'danhSachPhuongThucTuyenSinh']);
                    setNamValue('');
                    setPhuongThucTuyenSinhValue([]);
                  }}
                  allowClear
                >
                  {hinhThucDaoTaoDanhSach?.map((data: HinhThucDaoTao.Record) => {
                    return (
                      <Option key={data._id} value={data._id}>
                        {data.ten}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                label="Mã đợt tuyển sinh"
                name="maDotTuyenSinh"
                rules={[{ required: true }]}
                initialValue={edit ? initialValue?.maDotTuyenSinh : ''}
              >
                <InputNumber placeholder="Mã đợt tuyển sinh" style={{ width: '100%' }} min="0" />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                label="Năm tuyển sinh"
                name="namTuyenSinh"
                rules={[{ required: true }]}
                initialValue={edit ? initialValue?.namTuyenSinh : []}
              >
                <Select
                  placeholder="Năm tuyển sinh"
                  onChange={(...value: any[]) => {
                    setNamValue(value[1]?.key);
                    form.resetFields(['danhSachphuongThucTuyenSinh']);
                    setPhuongThucTuyenSinhValue([]);
                  }}
                  notFoundContent="Bạn chưa chọn hình thức đào tạo hoặc không có năm tuyển sinh nào cho hình thức đào tạo này"
                >
                  {_.filter(namTuyenSinhDanhSach, (data: NamTuyenSinh.Record) => {
                    return data?.hinhThucDaoTao?._id === hinhThucDaoTaoValue;
                  }).map((data: NamTuyenSinh.Record) => {
                    return (
                      <Option key={data._id} value={data.nam}>
                        {data.nam}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                label="Loại đợt"
                name="loaiDot"
                rules={[...rules.required]}
                initialValue={edit ? initialValue?.loaiDot : []}
              >
                <Select placeholder="Loại đợt">
                  <Option value="Đề án riêng">Đề án riêng</Option>
                  <Option value="Khác">Khác</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            label="Phương thức tuyển sinh"
            name="danhSachPhuongThucTuyenSinh"
            rules={[{ required: true }]}
            initialValue={
              edit ? initialValue?.danhSachPhuongThucTuyenSinh.map((data) => data?._id) : []
            }
          >
            <Select
              mode="multiple"
              placeholder="Phương thức tuyển sinh"
              notFoundContent="Bạn chưa chọn năm tuyển sinh"
              onChange={(value, option: any) => {
                setPhuongThucTuyenSinhValue(option);
              }}
            >
              {useMemo(()=> showDanhSachPhuongThuc(),[namValue, hinhThucDaoTaoValue])}
            </Select>
          </Form.Item>
          {useMemo(() => <DanhSachDoiTuongTable/>, [phuongThucTuyenSinhValue])}
          {useMemo(() => <DanhSachNganhTuyenSinhTable />, [danhSachNganhTuyenSinh])}
          {useMemo(() => <DanhSachGiayToNopHoSoTable />, [danhSachGiayToNopHoSo])}
          {useMemo(() => <DanhSachGiayToNopHoSoOnlineTable />, [danhSachGiayToNopHoSoOnline])}
          {useMemo(() => <DanhSachGiayToXacNhanTable />, [danhSachGiayToXacNhan])}
          {useMemo(() => <DanhSachThongTinKhaiXacNhanTable />, [danhSachThongTinKhaiXacNhan])}
          {useMemo(() => <DanhSachHuongDanSuDungTable />, [danhSachHuongDanSuDung])}
          <Form.Item
            label="Mô tả"
            name="moTa"
            rules={[...rules.required]}
            initialValue={{ text: edit ? initialValue?.moTa : ''}}
          >
            <TinyEditor />
          </Form.Item>
          <Row gutter={[16, 16]}>
            <Col span={12}>
              <Form.Item
                label="Thời gian mở đăng ký"
                name="thoiGianMoDangKy"
                rules={[...rules.required]}
                initialValue={edit ? moment(initialValue?.thoiGianMoDangKy) : ''}
              >
                <DatePicker
                  style={{ width: '100%' }}
                  format="HH:mm DD/MM/YYYY"
                  showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Thời gian kết thúc nộp hồ sơ"
                name="thoiGianKetThucNopHoSo"
                rules={[...rules.required]}
                initialValue={edit ? moment(initialValue?.thoiGianKetThucNopHoSo) : ''}
              >
                <DatePicker
                  style={{ width: '100%' }}
                  format="HH:mm DD/MM/YYYY"
                  showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Thời gian công bố kết quả"
                name="thoiGianCongBoKetQua"
                rules={[...rules.required]}
                initialValue={edit ? moment(initialValue?.thoiGianCongBoKetQua) : ''}
              >
                <DatePicker
                  style={{ width: '100%' }}
                  format="HH:mm DD/MM/YYYY"
                  showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Thời gian bắt đầu xác nhận nhập học"
                name="thoiGianBatDauXacNhanNhapHoc"
                rules={[...rules.required]}
                initialValue={edit ? moment(initialValue?.thoiGianBatDauXacNhanNhapHoc) : ''}
              >
                <DatePicker
                  style={{ width: '100%' }}
                  format="HH:mm DD/MM/YYYY"
                  showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Thời gian kết thúc xác nhận nhập học"
                name="thoiGianKetThucXacNhanNhapHoc"
                rules={[...rules.required]}
                initialValue={edit ? moment(initialValue?.thoiGianKetThucXacNhanNhapHoc) : ''}
              >
                <DatePicker
                  style={{ width: '100%' }}
                  format="HH:mm DD/MM/YYYY"
                  showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Số lượng nguyện vọng tối đa"
                name="soLuongNguyenVongToiDa"
                initialValue={edit ? initialValue?.soLuongNguyenVongToiDa : ''}
              >
                <InputNumber placeholder="Mã đợt tuyển sinh" style={{ width: '100%' }} min="0" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Row gutter={[16, 16]}>
                <Col span={12}>
                  <Form.Item
                    name="yeuCauTraPhi"
                    valuePropName="checked"
                    initialValue={edit ? initialValue?.yeuCauTraPhi : false}
                  >
                    <Checkbox
                      onChange={(e) => {
                        setNeedPay(e.target.checked);
                      }}
                    >
                      Cho phép thanh toán
                    </Checkbox>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="choPhepDangKyKhacCoSo"
                    valuePropName="checked"
                    initialValue={edit ? initialValue?.choPhepDangKyKhacCoSo : false}
                  >
                    <Checkbox
                      onChange={(e) => {
                        setDangKiCoSo(e.target.checked);
                      }}
                    >
                      Cho phép đăng ký khác cơ sở
                    </Checkbox>
                  </Form.Item>
                </Col>
                {needPay ? (
                  <>
                    <Col span={12}>
                      <Form.Item
                        name="idLePhi"
                        label="Chọn lệ phí"
                        rules={[...rules.required]}
                        initialValue={edit ? initialValue?.idLePhi : []}
                      >
                        <Select
                          placeholder="Chọn lệ phí"
                          onChange={(...params: any) => {
                            setAmount([].concat(params[1].amount));
                            setDonVi(params[1].donVi);
                          }}
                        >
                          {_.map(payMentDanhSach, (data) => {
                            const value = _.find(data?.prices, (prices) => prices.active === true);
                            return (
                              <Option
                                key={data?._id}
                                value={value?._id}
                                amount={value?.unitAmount}
                                donVi={data?.unitLabel}
                              >
                                {data?.name}
                              </Option>
                            );
                          })}
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="mucLePhi"
                        label="Mức lệ phí"
                        rules={[...rules.required]}
                        initialValue={amountInitial == 0 ? '' : amountInitial}
                      >
                        <Select placeholder="Chọn mức lệ phí">
                          {_.map(amount, (data, index) => {
                            return (
                              <Option key={index} value={data}>
                                {data}
                              </Option>
                            );
                          })}
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="donViTinh"
                        label="Đơn vị tính"
                        rules={[...rules.required]}
                        initialValue={donVi}
                      >
                        <Input placeholder="Hãy chọn mức lệ phí" readOnly />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="hinhThucThanhToan"
                        label="Hình thức thanh toán"
                        rules={[...rules.required]}
                        initialValue={edit ? initialValue?.hinhThucThanhToan : []}
                      >
                        <Select
                          options={Object.keys(EHinhThucThanhToan).map((item) => ({
                            value: item,
                            label: MapKeyHinhThucThanhToan[item],
                          }))}
                          placeholder="Hình thức thanh toán"
                          onChange={(val) => setHinhThucThanhToan(val)}
                        />
                      </Form.Item>
                    </Col>
                    {hinhThucThanhToan === EHinhThucThanhToan.TRUYEN_THONG && (
                      <Col span={24}>
                        <Form.Item
                          name={'huongDanThanhToan'}
                          label={`Hướng dẫn thanh toán`}
                          initialValue={{ text: initialValue?.huongDanThanhToan || '' }}
                        >
                          <TinyEditor />
                        </Form.Item>
                      </Col>
                    )}
                  </>
                ) : (
                  <></>
                )}
                <Col span={12}>
                  <Form.Item
                    name="gioiHanDoiTuong"
                    valuePropName="checked"
                    initialValue={edit ? initialValue?.gioiHanDoiTuong : false}
                  >
                    <Checkbox>Giới hạn đối tượng</Checkbox>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="suDungToHopMongMuon"
                    valuePropName="checked"
                    initialValue={edit ? initialValue?.suDungToHopMongMuon : false}
                  >
                    <Checkbox>Sử dụng tổ hợp mong muốn</Checkbox>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="choPhepHK1HoacCaNamLop12"
                    valuePropName="checked"
                    initialValue={edit ? initialValue?.choPhepHK1HoacCaNamLop12 : false}
                  >
                    <Checkbox>Cho phép HK1 hoặc cả năm Lớp 12</Checkbox>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="choPhepThiSinhMoKhoa"
                    valuePropName="checked"
                    initialValue={edit ? initialValue?.choPhepThiSinhMoKhoa : false}
                  >
                    <Checkbox>Cho phép thí sinh mở khóa hồ sơ</Checkbox>
                  </Form.Item>
                </Col>
                {!dangKiCoSo && (
                  <Col span={12}>
                    <Form.Item
                      name="choPhepGiaLapTheoCoSo"
                      valuePropName="checked"
                      initialValue={edit ? initialValue?.choPhepGiaLapTheoCoSo : false}
                    >
                      <Checkbox>Cho phép giả lập theo cơ sở</Checkbox>
                    </Form.Item>
                  </Col>
                )}
                <Col span={12}>
                  <Form.Item
                    name="duocPhepCapNhatSauHanNop"
                    valuePropName="checked"
                    initialValue={edit ? initialValue?.duocPhepCapNhatSauHanNop : false}
                  >
                    <Checkbox
                      onChange={(e) => {
                        setCapNhatHoSo(e.target.checked);
                      }}
                    >
                      Cho phép cập nhật hồ sơ sau hạn nộp
                    </Checkbox>
                  </Form.Item>
                </Col>
                {capNhatHoSo && (
                  <Col span={12}>
                    <Form.Item
                      label="Thời gian kết thúc cập nhật sau hạn nộp"
                      name="thoiGianKetThucCapNhatSauHanNop"
                      rules={[...rules.required]}
                      initialValue={
                        edit ? moment(initialValue?.thoiGianKetThucCapNhatSauHanNop) : ''
                      }
                    >
                      <DatePicker
                        style={{ width: '100%' }}
                        format="HH:mm DD/MM/YYYY"
                        showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
                      />
                    </Form.Item>
                  </Col>
                )}
                <Col span={12}>
                  <Form.Item
                    label="Mẫu phiếu đăng ký"
                    name="mauPhieuDangKy"
                    initialValue={
                      edit ? renderFileList([initialValue.mauPhieuDangKy?.url || ''] ?? []) : []
                    }
                  >
                    <Upload
                      otherProps={{
                        accept: 'application/pdf, image/png, .jpg, .doc, .docx',
                        multiple: false,
                        showUploadList: { showDownloadIcon: false },
                      }}
                      limit={1}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Row>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <Space>
              <Button htmlType="submit" type="primary">
                Lưu
              </Button>
              <Button onClick={() => setVisibleForm(false)}>Đóng</Button>
            </Space>
          </div>
        </Form>
      </Row>
    </>
  );
};

export default MainForm;
