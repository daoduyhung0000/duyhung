import rules from '@/utils/rules';
import { Button, Col, Form, Radio, Row, Select, Space } from 'antd';
import { useEffect, useMemo, useState } from 'react';
import { useModel } from 'umi';
import TextArea from 'antd/lib/input/TextArea';


const DanhSachDoiTuongForm: React.FC<ChildProps.Props> = (props) => {
  const [form] = Form.useForm();
  const [isPreview, setIsPreview] = useState<boolean>(false);
  const [maDoiTuong, setMaDoiTuong] = useState<string>('');
  const { Option } = Select;

  const { 
    isEdit, 
    initialValue, 
    setShowForm 
  } = props;

  const {
    hinhThucDaoTaoValue,
    doiTuongTuyenSinhDanhSach,
    phuongThucTuyenSinhValue,
    danhSachDoiTuong,
    setDanhSachDoiTuong,
  } = useModel('qldottuyensinh');
  
  
  useEffect(() => {
    if (!hinhThucDaoTaoValue || hinhThucDaoTaoValue == '') form.resetFields(['thongTinDoiTuong']);
    if (!phuongThucTuyenSinhValue || phuongThucTuyenSinhValue == [])
      form.resetFields(['phuongThucTuyenSinh']);
    if (initialValue !== {}) form.resetFields();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    hinhThucDaoTaoValue, 
    phuongThucTuyenSinhValue, 
    initialValue
  ]);

  function doiTuongTuyenSinhList() {
    
    const listDoiTuongMap = doiTuongTuyenSinhDanhSach.filter((doiTuongVal) => {
      return doiTuongVal?.hinhThucDaoTao?._id === hinhThucDaoTaoValue
    })
    
    return listDoiTuongMap.map((thongTinDoiTuong) => {
      return (
        <Option
          value={JSON.stringify({
            hinhThucDaoTao: thongTinDoiTuong?.hinhThucDaoTao,
            maDoiTuong: thongTinDoiTuong?.maDoiTuong,
            moTa: '',
            tenDoiTuong: thongTinDoiTuong?.tenDoiTuong,
          })}
          key={thongTinDoiTuong?._id}
        >
          {thongTinDoiTuong?.tenDoiTuong}
        </Option>
      );
    })
  }

  function showPhuongThucTuyenSinh() {
    return phuongThucTuyenSinhValue.map((phuongThucVal: PhuongThucTuyenSinhValue) => {
      return (
        <Option value={phuongThucVal?.value} key={phuongThucVal?.key}>
          {phuongThucVal?.name}
        </Option>
      );
    })
  }
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={(values: any) => {
        values.maDoiTuong = maDoiTuong;
        values.thongTinDoiTuong = JSON.parse(values.thongTinDoiTuong);
        values.cauHinhQuyDoi = JSON.parse(values.cauHinhQuyDoi);
        values.cauHinhValidateTheoNganhToHopCoSo = JSON.parse(
          values.cauHinhValidateTheoNganhToHopCoSo,
        );
        values.cauHinhDoiTuong = JSON.parse(values.cauHinhDoiTuong);
        values.congThucPreviewQuyDoi = JSON.parse(values.congThucPreviewQuyDoi || '{}');
        if (!isEdit) {
          const copy = [...danhSachDoiTuong];
          setDanhSachDoiTuong(copy.concat(values));
        } else {
          const copy = [...danhSachDoiTuong];
          copy[initialValue.index] = values;
          setDanhSachDoiTuong(copy);
        }
        setShowForm(false);
      }}
    >
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name="thongTinDoiTuong"
            label="Đối tượng tuyển sinh"
            rules={[...rules.required]}
            initialValue={isEdit ? JSON.stringify(initialValue?.object?.thongTinDoiTuong) : []}
          >
            <Select
              placeholder="Đối tượng tuyển sinh"
              notFoundContent="Bạn chưa chọn hình thức đào tạo hoặc không có đối tượng tuyển sinh nào cho hình thức đào tạo này"
              onChange={(value) => {
                setMaDoiTuong(JSON.parse(value).maDoiTuong);
              }}
            >
              {
                // eslint-disable-next-line react-hooks/exhaustive-deps
                useMemo(() => doiTuongTuyenSinhList(), [doiTuongTuyenSinhDanhSach])
              }
            </Select>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="phuongThucTuyenSinh"
            label="Phương thức tuyển sinh"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.phuongThucTuyenSinh : []}
          >
            <Select placeholder="Phương thức tuyển sinh">
              {
                // eslint-disable-next-line react-hooks/exhaustive-deps
                useMemo(() => showPhuongThucTuyenSinh(),[phuongThucTuyenSinhValue])
              }
            </Select>
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item
            name="yeuCauLuaChonToHop"
            label="Yêu cầu lựa chọn tổ hợp"
            initialValue={isEdit ? initialValue?.object?.yeuCauLuaChonToHop : false}
          >
            <Radio.Group>
              <Radio value={true}>Có</Radio>
              <Radio value={false}>Không</Radio>
            </Radio.Group>
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item
            name="hienThiDiemQuyDoi"
            label="Hiển thị điểm quy đổi"
            initialValue={isEdit ? initialValue?.object?.hienThiDiemQuyDoi : false}
          >
            <Radio.Group>
              <Radio value={true}>Có</Radio>
              <Radio value={false}>Không</Radio>
            </Radio.Group>
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item
            name="hienThiPreviewDiemQuyDoi"
            label="Hiển thị preview điểm quy đổi"
            initialValue={isEdit ? initialValue?.object?.hienThiPreviewDiemQuyDoi : false}
          >
            <Radio.Group
              onChange={(e) => {
                setIsPreview(e.target.value);
              }}
            >
              <Radio value={true}>Có</Radio>
              <Radio value={false}>Không</Radio>
            </Radio.Group>
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="cauHinhDoiTuong"
            label="Cấu hình đối tượng"
            rules={[...rules.required]}
            initialValue={'{}'}
          >
            <TextArea style={{ width: '100%' }} rows={3} />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="cauHinhQuyDoi"
            label="Cấu hình công thức quy đổi"
            rules={[...rules.required]}
            initialValue={'[]'}
          >
            <TextArea style={{ width: '100%' }} rows={3} />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="cauHinhValidateTheoNganhToHopCoSo"
            label="Cấu hình điều kiện theo nghành"
            rules={[...rules.required]}
            initialValue={'[]'}
          >
            <TextArea style={{ width: '100%' }} rows={3} />
          </Form.Item>
        </Col>
        {isPreview ? (
          <Col span={24}>
            <Form.Item
              name="congThucPreviewQuyDoi"
              label="Cấu hình preview quy đổi"
              rules={[...rules.required]}
              initialValue={'{}'}
            >
              <TextArea style={{ width: '100%' }} rows={3} />
            </Form.Item>
          </Col>
        ) : (
          ''
        )}
      </Row>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Space>
          <Button htmlType="submit" type="primary">
            Lưu
          </Button>
          <Button onClick={() => setShowForm(false)}>Đóng</Button>
        </Space>
      </div>
    </Form>
  );
};

export default DanhSachDoiTuongForm;
