import Upload from '@/components/Upload/UploadMultiFile';
import rules from '@/utils/rules';
import { uploadMultiFile, renderFileList } from '@/utils/utils';
import { Button, Col, Form, Input, Row, Space } from 'antd';
import { useEffect } from 'react';
import { useModel } from 'umi';

const DanhSachHuongDanSuDungForm: React.FC<ChildProps.Props> = (props) => {
  const [form] = Form.useForm();
  const { 
    setShowForm, 
    isEdit, 
    initialValue 
  } = props;

  const { 
    danhSachHuongDanSuDung, 
    setDanhSachHuongDanSuDung 
  } = useModel('qldottuyensinh');

  useEffect(() => {
    form.resetFields();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isEdit, initialValue]);
  
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={async (values) => {
        const url = await uploadMultiFile(values?.tepDinhKem?.fileList);
        values.tepDinhKem = url.toString();
        if (!isEdit) {
          setDanhSachHuongDanSuDung(danhSachHuongDanSuDung.concat(values));
        } else {
          const copy = [...danhSachHuongDanSuDung];
          copy[initialValue.index] = values;
          setDanhSachHuongDanSuDung(copy);
        }

        setShowForm(false);
      }}
    >
      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="tenHuongDan"
            label="Tên hướng dẫn"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.tenHuongDan : ''}
          >
            <Input placeholder="Tên hướng dẫn" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            label="Tệp đính kèm"
            name="tepDinhKem"
            initialValue={renderFileList(initialValue?.object?.urlHuongDan ?? '')}
          >
            <Upload
              otherProps={{
                accept: 'application/pdf, .doc, .docx',
                multiple: true,
                showUploadList: { showDownloadIcon: false },
              }}
              limit={1}
            />
          </Form.Item>
        </Col>
      </Row>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Space>
          <Button htmlType="submit" type="primary">
            Lưu
          </Button>
          <Button onClick={() => setShowForm(false)}>Đóng</Button>
        </Space>
      </div>
    </Form>
  );
};

export default DanhSachHuongDanSuDungForm;
