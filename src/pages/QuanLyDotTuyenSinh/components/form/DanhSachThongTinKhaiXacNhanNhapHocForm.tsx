import Upload from '@/components/Upload/UploadMultiFile';
import rules from '@/utils/rules';
import { uploadMultiFile, renderFileList } from '@/utils/utils';
import { Button, Col, Form, Input, Radio, Row, Space } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { useEffect } from 'react';
import { useModel } from 'umi';

const DanhSachThongTinKhaiXacNhanForm: React.FC<ChildProps.Props> = (props) => {
  const [form] = Form.useForm();
  const { 
    setShowForm, 
    isEdit, 
    initialValue 
  } = props;

  const { 
    danhSachThongTinKhaiXacNhan, 
    setDanhSachThongTinKhaiXacNhan 
  } =useModel('qldottuyensinh');
  
  useEffect(() => {
    form.resetFields();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isEdit, initialValue]);
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={async (values) => {
        const urlHuongDan = await uploadMultiFile(values?.urlHuongDan?.fileList);
        values.urlHuongDan = urlHuongDan;
        if (!isEdit) {
          setDanhSachThongTinKhaiXacNhan(danhSachThongTinKhaiXacNhan.concat(values));
        } else {
          const copy = [...danhSachThongTinKhaiXacNhan];
          copy[initialValue.index] = values;
          setDanhSachThongTinKhaiXacNhan(copy);
        }

        setShowForm(false);
      }}
    >
      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="maThongTin"
            label="Mã thông tin"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.maThongTin : ''}
          >
            <Input placeholder="Mã thông tin" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="tieuDe"
            label="Tiêu đề"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.tieuDe : ''}
          >
            <Input placeholder="Tiêu đề" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="required"
            label="Bắt buộc"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.required : false}
          >
            <Radio.Group value={false}>
              <Radio value={true}>Có</Radio>
              <Radio value={false}>Không</Radio>
            </Radio.Group>
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="textHuongDan"
            label="Hướng dẫn"
            initialValue={isEdit ? initialValue?.object?.textHuongDan : ''}
          >
            <TextArea style={{ width: '100%' }} rows={3} />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            extra={<div>Tối đa 5 file, kích thước mỗi file không quá 8MB.</div>}
            label="File hướng dẫn"
            name="urlHuongDan"
            initialValue={renderFileList(initialValue?.object?.urlHuongDan ?? [])}
          >
            <Upload
              otherProps={{
                accept: 'application/pdf, image/png, .jpg, .doc, .docx',
                multiple: true,
                showUploadList: { showDownloadIcon: false },
              }}
              limit={5}
            />
          </Form.Item>
        </Col>
      </Row>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Space>
          <Button htmlType="submit" type="primary">
            Lưu
          </Button>
          <Button onClick={() => setShowForm(false)}>Đóng</Button>
        </Space>
      </div>
    </Form>
  );
};

export default DanhSachThongTinKhaiXacNhanForm;
