import { Button, Col, Form, Row, Select, Space } from 'antd';
import { useModel } from 'umi';
import _ from 'lodash';
import rules from '@/utils/rules';
import { ToHopXetTuyen } from '@/utils/constants';
import { useEffect, useMemo } from 'react';
const DanhSachNganhTuyenSinhForm: React.FC<ChildProps.Props> = (props) => {
  const { Option } = Select;
  const [form] = Form.useForm();

  const { 
    setShowForm, 
    isEdit, 
    initialValue 
  } = props;

  const {
    coSoDaoTaoDanhSach,
    nganhChuyenNganhDanhSach,
    danhSachNganhTuyenSinh,
    setDanhSachNganhTuyenSinh,
  } = useModel('qldottuyensinh');
  
  useEffect(() => {
    if (initialValue !== {}) form.resetFields();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialValue]);

  const listCoSoDaoTao = () => {
    return coSoDaoTaoDanhSach.map(data => {
      return (
        <Option key={data?._id} value={data?._id}>
          {data?.ten}
        </Option>
      );
    })
  }

  const listToHop = () => {
    const toHopArr = _.toPairs(ToHopXetTuyen)
    return toHopArr.map((data) => {
      return (
        <Option 
          key={data[0]} 
          value={data[0]}
        >
          {`${data[0]}(${data[1].toString()})`}
        </Option>
      );
    })
  }
  
  const listNganhChuyenNganh = () => {
    return nganhChuyenNganhDanhSach.map(data => {
      return (
        <Option 
          key={data?._id} 
          value={data?._id}
        >
          {data?.ten}
        </Option>
      );
    })
  }
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={(values: any) => {
        if (!isEdit) {
          setDanhSachNganhTuyenSinh(danhSachNganhTuyenSinh.concat(values));
        } else {
          const copy = [...danhSachNganhTuyenSinh];
          copy[initialValue.index] = values;
          setDanhSachNganhTuyenSinh(copy);
        }
        setShowForm(false);
      }}
    >
      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="danhSachCoSoDaoTao"
            label="Cơ sở đào tạo"
            initialValue={isEdit ? initialValue?.object?.danhSachCoSoDaoTao : []}
            rules={[...rules.required]}
          >
            <Select mode="multiple" placeholder="Chọn cơ sở đào tạo">
              {
                // eslint-disable-next-line react-hooks/exhaustive-deps
                useMemo(() => listCoSoDaoTao(), [coSoDaoTaoDanhSach])
              }
            </Select>
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="nganh"
            label="Ngành xét tuyển"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.nganh : ''}
          >
            <Select placeholder="Chọn ngành tuyển sinh">
              {
                // eslint-disable-next-line react-hooks/exhaustive-deps
                useMemo(() => listNganhChuyenNganh(), [nganhChuyenNganhDanhSach])
              }
            </Select>
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="danhSachToHop"
            label="Tổ hợp xét tuyển"
            initialValue={isEdit ? initialValue.object.danhSachToHop : []}
          >
            <Select mode="multiple" placeholder="Chọn tổ hợp xét tuyển">
              {
                listToHop()
              }
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Space>
          <Button htmlType="submit" type="primary">
            Lưu
          </Button>
          <Button onClick={() => setShowForm(false)}>Đóng</Button>
        </Space>
      </div>
    </Form>
  );
};

export default DanhSachNganhTuyenSinhForm;
