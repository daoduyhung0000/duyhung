import Upload from '@/components/Upload/UploadMultiFile';
import rules from '@/utils/rules';
import { uploadMultiFile, renderFileList } from '@/utils/utils';
import { Button, Col, Form, Input, InputNumber, Radio, Row, Space } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { useEffect } from 'react';
import { useModel } from 'umi';

const DanhSachGiayToNopHoSoForm: React.FC<ChildProps.Props> = (props) => {
  const [form] = Form.useForm();
  const { 
    setShowForm, 
    isEdit, 
    initialValue 
  } = props;

  const { 
    danhSachGiayToNopHoSo, 
    setDanhSachGiayToNopHoSo 
  } = useModel('qldottuyensinh');

  
  useEffect(() => {
    form.resetFields();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    isEdit, 
    initialValue
  ]);
  
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={async (values) => {
        const urlHuongDan = await uploadMultiFile(values?.urlHuongDan?.fileList);
        values.urlHuongDan = urlHuongDan;
        if (!isEdit) {
          setDanhSachGiayToNopHoSo(danhSachGiayToNopHoSo.concat(values));
        } else {
          const copy = [...danhSachGiayToNopHoSo];
          copy[initialValue.index] = values;
          setDanhSachGiayToNopHoSo(copy);
        }

        setShowForm(false);
      }}
    >
      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="maGiayTo"
            label="Mã giấy tờ"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.maGiayTo : ''}
          >
            <Input placeholder="Mã giấy tờ" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="ten"
            label="Tên giấy tờ"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.ten : ''}
          >
            <Input placeholder="Tên giấy tờ" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="soLuong"
            label="Số lượng"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.soLuong : 0}
          >
            <InputNumber min={1} />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="required"
            label="Bắt buộc nộp"
            rules={[...rules.required]}
            initialValue={isEdit ? initialValue?.object?.required : false}
          >
            <Radio.Group value={false}>
              <Radio value={true}>Có</Radio>
              <Radio value={false}>Không</Radio>
            </Radio.Group>
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="ghiChu"
            label="Ghi chú"
            initialValue={isEdit ? initialValue?.object?.ghiChu : ''}
          >
            <TextArea style={{ width: '100%' }} rows={3} />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            name="textHuongDan"
            label="Hướng dẫn"
            initialValue={isEdit ? initialValue?.object?.textHuongDan : ''}
          >
            <TextArea style={{ width: '100%' }} rows={3} />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item
            extra={<div>Tối đa 5 file, kích thước mỗi file không quá 8MB.</div>}
            label="File hướng dẫn"
            name="urlHuongDan"
            initialValue={renderFileList(initialValue?.object?.urlHuongDan ?? [])}
          >
            <Upload
              otherProps={{
                accept: 'application/pdf, image/png, .jpg, .doc, .docx',
                multiple: true,
                showUploadList: { showDownloadIcon: false },
              }}
              limit={5}
            />
          </Form.Item>
        </Col>
      </Row>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Space>
          <Button htmlType="submit" type="primary">
            Lưu
          </Button>
          <Button onClick={() => setShowForm(false)}>Đóng</Button>
        </Space>
      </div>
    </Form>
  );
};

export default DanhSachGiayToNopHoSoForm;
