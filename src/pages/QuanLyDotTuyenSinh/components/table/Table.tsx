import { Button, Modal, Table } from 'antd';

const TableChild: React.FC<any> = (props) => {
  const { title, nameForm, columns, Form, showForm, setShowForm, data, setIsEdit } = props;
  return (
    <>
      <h1 style={{ fontSize: '1rem' }}>{title}</h1>
      <Button
        type="primary"
        onClick={() => {
          setIsEdit(false);
          setShowForm(true);
        }}
      >
        Thêm mới
      </Button>
      <Table columns={columns} style={{ marginBottom: '1rem' }} dataSource={data} />
      <Modal
        title={nameForm}
        visible={showForm}
        onCancel={() => {
          setShowForm(false);
        }}
        footer={null}
        width={800}
        destroyOnClose
      >
        <Form {...props} />
      </Modal>
    </>
  );
};

export default TableChild;
