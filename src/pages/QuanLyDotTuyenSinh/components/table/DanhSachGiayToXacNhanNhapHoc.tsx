import {
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  QuestionCircleOutlined,
} from '@ant-design/icons';
import { Button, Col, Divider, Modal, Row } from 'antd';
import { useState } from 'react';
import { useModel } from 'umi';
import DanhSachGiayToXacNhanForm from '../form/DanhSachGiayToXacNhanNhapHocForm';
import Table from './Table';

const DanhSachGiayToXacNhanTable: React.FC = () => {
  const [showForm, setShowForm] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [initialValue, setInitialValue] = useState<InitialValue<QLDotTuyenSinh.DanhSachGiayToXacNhanNhapHoc[]>>({});
  const [modalUrl, setModalUrl] = useState<boolean>(false);
  const { 
    danhSachGiayToXacNhan, 
    setDanhSachGiayToXacNhan 
  } = useModel('qldottuyensinh');

  
  const columns = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      align: 'center',
      render: (...params: any[]) => {
        return params[2] + 1;
      },
    },
    {
      title: 'Tên',
      dataIndex: 'tieuDe',
      key: 'ten',
      align: 'center',
      render: (...params: any[]) => {
        return (
          <>
            <span>{params[0]}</span>
            <QuestionCircleOutlined
              onClick={() => {
                setModalUrl(true);
              }}
              style={{ marginLeft: '0.5rem' }}
            />
            <Modal visible={modalUrl} footer={null} onCancel={() => setModalUrl(false)}>
              <Row gutter={16}>
                <Col span={2}>
                  <ExclamationCircleOutlined />
                </Col>
                <Col span={22}>
                  <p>File hướng dẫn đính kèm:</p>
                  {params[1].urlHuongDan.map((data: string , index: number) => {
                    return (
                      <a key={index} href={data} target="_blank" rel="noreferrer">
                        {`Xem tập tin ${index + 1}`}
                      </a>
                    );
                  })}
                </Col>
              </Row>
            </Modal>
          </>
        );
      },
    },
    {
      title: 'Bắt buộc',
      dataIndex: 'required',
      key: 'batBuoc',
      align: 'center',
      render: (data: boolean) => {
        return <div>{data ? 'Có' : 'Không'}</div>;
      },
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      align: 'center',
      render: (obj: QLDotTuyenSinh.DanhSachGiayToXacNhanNhapHoc[], record: QLDotTuyenSinh.DanhSachGiayToXacNhanNhapHoc, index: number) => {
        return (
          <>
            <Button
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                setInitialValue({ object: obj, index: index });
                setIsEdit(true);
                setShowForm(true);
              }}
            />
            <Divider type="vertical" />
            <Button
              shape="circle"
              icon={<DeleteOutlined />}
              type="primary"
              onClick={() => {
                const copy = [...danhSachGiayToXacNhan];
                copy.splice(index, 1);
                setDanhSachGiayToXacNhan(copy);
              }}
            />
          </>
        );
      },
    },
  ];
  const tableProperties = {
    columns: columns,
    title: 'Danh sách giấy tờ xác nhận nhập học',
    nameForm: isEdit ? 'Chỉnh sửa giấy tờ' : 'Thêm mới giấy tờ',
    Form: DanhSachGiayToXacNhanForm,
    showForm: showForm,
    setShowForm: setShowForm,
    data: danhSachGiayToXacNhan,
    isEdit: isEdit,
    setIsEdit: setIsEdit,
    initialValue: initialValue,
    setInitialValue: setInitialValue,
  };
  return <Table {...tableProperties} />;
};

export default DanhSachGiayToXacNhanTable;
