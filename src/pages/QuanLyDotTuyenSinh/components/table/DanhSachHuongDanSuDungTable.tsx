import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Divider } from 'antd';
import { useState } from 'react';
import { useModel } from 'umi';
import DanhSachHuongDanSuDungForm from '../form/DanhSachHuongDanSuDungForm';
import Table from './Table';

const DanhSachHuongDanSuDungTable: React.FC = () => {
  const { danhSachHuongDanSuDung, setDanhSachHuongDanSuDung } = useModel('qldottuyensinh');
  const [showForm, setShowForm] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [initialValue, setInitialValue] = useState<InitialValue<QLDotTuyenSinh.DanhSachHuongDanSuDung[]>>({});
  const columns = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      align: 'center',
      render: (...params: any[]) => {
        return params[2] + 1;
      },
    },
    {
      title: 'Tên hướng dẫn',
      dataIndex: 'tenHuongDan',
      align: 'center',
      key: 'ten',
    },
    {
      title: 'Tệp đính kèm',
      dataIndex: 'tepDinhKem',
      key: 'tepDinhKem',
      align: 'center',
      render: (data: string) => {
        return (
          <a href={data} target="_blank" rel="noreferrer">
            {data?.split('/')?.pop()}
          </a>
        );
      },
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      align: 'center',
      render: (obj: QLDotTuyenSinh.DanhSachHuongDanSuDung[], record: QLDotTuyenSinh.DanhSachHuongDanSuDung, index: number) => {
        return (
          <>
            <Button
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                setInitialValue({ object: obj, index: index });
                setIsEdit(true);
                setShowForm(true);
              }}
            />
            <Divider type="vertical" />
            <Button
              shape="circle"
              icon={<DeleteOutlined />}
              type="primary"
              onClick={() => {
                const copy = [...danhSachHuongDanSuDung];
                copy.splice(index, 1);
                setDanhSachHuongDanSuDung(copy);
              }}
            />
          </>
        );
      },
    },
  ];
  const tableProperties = {
    columns: columns,
    title: 'Danh sách hướng dẫn sử dụng',
    nameForm: isEdit ? 'Chỉnh sửa ' : 'Thêm mới ',
    Form: DanhSachHuongDanSuDungForm,
    showForm: showForm,
    setShowForm: setShowForm,
    data: danhSachHuongDanSuDung,
    isEdit: isEdit,
    setIsEdit: setIsEdit,
    initialValue: initialValue,
    setInitialValue: setInitialValue,
  };
  return <Table {...tableProperties} />;
};

export default DanhSachHuongDanSuDungTable;
