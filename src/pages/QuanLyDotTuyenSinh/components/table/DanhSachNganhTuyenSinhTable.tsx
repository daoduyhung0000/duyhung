import { Button, Divider, Tag } from 'antd';
import { useState } from 'react';
import { useModel } from 'umi';
import DanhSachNganhTuyenSinhForm from '../form/DanhSachNganhTuyenSinhForm';
import Table from './Table';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
const DanhSachNganhTuyenSinhTable: React.FC = () => {
  const {
    danhSachNganhTuyenSinh,
    setDanhSachNganhTuyenSinh,
    nganhChuyenNganhDanhSach,
    coSoDaoTaoDanhSach,
  } = useModel('qldottuyensinh');
  const [showForm, setShowForm] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [initialValue, setInitialValue] = useState<InitialValue<QLDotTuyenSinh.DanhSachNganhTuyenSinh[]>>({});
  const columns = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      align: 'center',
      render: (...params: any[]) => {
        return params[2] + 1;
      },
    },
    {
      title: 'Tên ngành',
      dataIndex: 'nganh',
      key: 'tenNganh',
      align: 'center',
      render: (data: NganhChuyenNganh.Record ) => {
        const element = nganhChuyenNganhDanhSach.find(val => {
          return val?._id === data || data?._id;
        })
        return <>{element?.ten}</>;
      },
    },
    {
      title: 'Tổ hợp',
      dataIndex: 'danhSachToHop',
      key: 'toHop',
      align: 'center',
      render: (data: string[]) => {
        return data?.toString();
      },
    },
    {
      title: 'Cơ sở đào tạo',
      dataIndex: 'danhSachCoSoDaoTao',
      key: 'coSoDaoTao',
      align: 'center',
      render: (data: string[]) => {
        return data.map((id) => {
          const element = coSoDaoTaoDanhSach.find((val) => val?._id === id)
          return <Tag key={element?._id}>{element?.ten}</Tag>;
        })
      },
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      align: 'center',
      render: (obj: QLDotTuyenSinh.DanhSachNganhTuyenSinh[], record: QLDotTuyenSinh.DanhSachNganhTuyenSinh, index: number) => {
        return (
          <>
            <Button
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                setInitialValue({ object: obj, index: index });
                setIsEdit(true);
                setShowForm(true);
              }}
            />
            <Divider type="vertical" />
            <Button
              shape="circle"
              icon={<DeleteOutlined />}
              type="primary"
              onClick={() => {
                const copy = [...danhSachNganhTuyenSinh];
                copy.splice(index, 1);
                setDanhSachNganhTuyenSinh(copy);
              }}
            />
          </>
        );
      },
      width: 200,
    },
  ];
  const tableProperties = {
    columns: columns,
    title: 'Danh sách ngành tuyển sinh',
    nameForm: isEdit ? 'Chỉnh sửa ngành' : 'Thêm mới ngành',
    Form: DanhSachNganhTuyenSinhForm,
    showForm: showForm,
    setShowForm: setShowForm,
    data: danhSachNganhTuyenSinh,
    isEdit: isEdit,
    setIsEdit: setIsEdit,
    initialValue: initialValue,
    setInitialValue: setInitialValue,
  };

  return <Table {...tableProperties} />;
};

export default DanhSachNganhTuyenSinhTable;
