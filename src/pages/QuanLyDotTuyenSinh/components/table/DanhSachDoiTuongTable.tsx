import DanhSachDoiTuongForm from '../form/DanhSachDoiTuongForm';
import TableChild from './Table';
import { useModel } from 'umi';
import { Button, Divider } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useState } from 'react';
import _ from 'lodash';


const DanhSachDoiTuongTable: React.FC = () => {
  const [showForm, setShowForm] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [initialValue, setInitialValue] = useState<InitialValue<QLDotTuyenSinh.DanhSachDoiTuongTuyenSinh[]>>({});

  const { 
    phuongThucTuyenSinhValue, 
    danhSachDoiTuong, 
    setDanhSachDoiTuong 
  } =useModel('qldottuyensinh');
  const columns = [
    {
      title: 'STT',
      key: 'stt',
      align: 'center',
      width: 80,
      render: (...params: any[]) => {
        return params[2] + 1;
      },
    },
    {
      title: 'Mã đối tượng',
      dataIndex: 'maDoiTuong',
      key: 'maDoiTuong',
      align: 'center',
      width: 100,
    },
    {
      title: 'Tên đối tượng',
      dataIndex: 'thongTinDoiTuong',
      key: 'tenDoiTuong',
      align: 'center',
      width: 150,
      render: (data: DoiTuongTuyenSinh.Record) => {
        return data.tenDoiTuong;
      },
    },
    {
      title: 'Phương thức tuyển sinh',
      dataIndex: 'phuongThucTuyenSinh',
      key: 'phuongThucTuyenSinh',
      align: 'center',
      width: 150,
      render: (data: NamTuyenSinh.PhuongThucTuyenSinh) => {
        const index = _.findIndex(phuongThucTuyenSinhValue, (val) => {
          if (val) {
            return val?.value === data;
          }
        });
        return phuongThucTuyenSinhValue[index]?.name;
      },
    },
    {
      title: 'Yêu cầu lựa chọn tổ hợp',
      dataIndex: 'yeuCauLuaChonToHop',
      key: 'yeuCauLuaChonToHop',
      align: 'center',
      width: 100,
      render: (data: boolean) => {
        return data ? 'Có' : 'Không';
      },
    },
    {
      title: 'Hiển thị điểm quy đổi',
      dataIndex: 'hienThiDiemQuyDoi',
      key: 'hienThiDiemQuyDoi',
      align: 'center',
      width: 100,
      render: (data: boolean) => {
        return data ? 'Có' : 'Không';
      },
    },
    {
      title: 'Hiển thị preview điểm quy đổi',
      dataIndex: 'hienThiPreviewDiemQuyDoi',
      key: 'hienThiPreviewDiemQuyDoi',
      align: 'center',
      width: 100,
      render: (data: boolean) => {
        return data ? 'Có' : 'Không';
      },
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      render: (obj: QLDotTuyenSinh.DanhSachDoiTuongTuyenSinh[] , record: QLDotTuyenSinh.DanhSachDoiTuongTuyenSinh, index: number) => {
        return (
          <>
            <Button
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                setInitialValue({ object: obj, index: index });
                setIsEdit(true);
                setShowForm(true);
              }}
            />
            <Divider type="vertical" />
            <Button
              shape="circle"
              icon={<DeleteOutlined />}
              type="primary"
              onClick={() => {
                const copy = [...danhSachDoiTuong];
                copy.splice(index, 1);
                setDanhSachDoiTuong(copy);
              }}
            />
          </>
        );
      },
      align: 'center',
      width: 200,
    },
  ];
  const TableProperties = {
    title: 'Danh sách đối tượng',
    columns: columns,
    nameForm: isEdit ? 'Chỉnh sửa danh sách đối tượng' : 'Thêm danh sách đối tượng',
    Form: DanhSachDoiTuongForm,
    showForm: showForm,
    setShowForm: setShowForm,
    data: danhSachDoiTuong,
    isEdit: isEdit,
    setIsEdit: setIsEdit,
    initialValue: initialValue,
    setInitialValue: setInitialValue,
  };
  return <TableChild {...TableProperties} />;
};

export default DanhSachDoiTuongTable;
