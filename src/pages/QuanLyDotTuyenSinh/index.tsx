import TableBase from '@/components/Table';
import {
  getAllCoSoDaoTao,
  getAllDoiTuongTuyenSinh,
  getAllHinhThucDaoTao,
  getAllNamTuyenSinh,
  getAllNganhChuyenNganh,
  getPayMent,
} from '@/services/QLDotTuyenSinh/qldottuyensinh';
import moment from 'moment';
import { useModel } from 'umi';
import MainForm from './components/Form';
import { Button, Divider, Popconfirm } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
const QuanLyDotTuyenSinh: React.FC = () => {
  const {
    getModel,
    limit,
    page,
    condition,
    loading,
    getData,
    deleteModel,
    setVisibleForm,
    setEdit,
    setInitialValue,
  } = useModel('qldottuyensinh');
  const columns = [
    {
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      width: 80,
    },
    {
      title: 'Tên đợt tuyển sinh',
      dataIndex: 'tenDotTuyenSinh',
      key: 'tenDotTuyenSinh',
      width: 200,
      align: 'center',
    },
    {
      title: 'Năm tuyển sinh',
      dataIndex: 'namTuyenSinh',
      key: 'namTuyenSinh',
      width: 120,
      align: 'center',
    },
    {
      title: 'Đối tượng tuyển sinh',
      dataIndex: 'danhSachDoiTuongTuyenSinh',
      key: 'anhSachDoiTuongTuyenSinh',
      render: (_arr: QLDotTuyenSinh.Record[], obj: QLDotTuyenSinh.Record) => {
        const listItems = obj?.danhSachDoiTuongTuyenSinh?.map(
          (data: QLDotTuyenSinh.DanhSachDoiTuongTuyenSinh, index: number) => {
            return (
              <p key={index} style={{ textAlign: 'left' }}>
                {'-' + data?.thongTinDoiTuong?.tenDoiTuong}
              </p>
            );
          },
        );
        return <div>{listItems}</div>;
      },
      width: 300,
      align: 'center',
    },
    {
      title: 'Thời gian mở đăng ký',
      dataIndex: 'thoiGianMoDangKy',
      key: 'thoiGianMoDangKy',
      render: (_arr: QLDotTuyenSinh.Record[], obj: QLDotTuyenSinh.Record) => {
        return moment(obj?.thoiGianMoDangKy).format(`DD/MM/YYYY H:mm`);
      },
      align: 'center',
      width: 150,
    },
    {
      title: 'Thời gian kết thúc nộp hồ sơ',
      dataIndex: 'thoiGianKetThucNopHoSo',
      key: 'thoiGianKetThucNopHoSo',
      render: (_arr: QLDotTuyenSinh.Record[], obj: QLDotTuyenSinh.Record) => {
        return moment(obj?.thoiGianMoDangKy).format(`DD/MM/YYYY H:mm`);
      },
      align: 'center',
      width: 150,
    },
    {
      title: 'Thời gian công bố kết quả',
      dataIndex: 'thoiGianCongBoKetQua',
      key: 'thoiGianCongBoKetQua',
      render: (_arr: QLDotTuyenSinh.Record[], obj: QLDotTuyenSinh.Record) => {
        return moment(obj?.thoiGianMoDangKy).format(`DD/MM/YYYY H:mm`);
      },
      align: 'center',
      width: 150,
    },
    {
      title: 'Thời gian kết thúc xác nhận nhập học',
      dataIndex: 'thoiGianKetThucXacNhanNhapHoc',
      key: 'thoiGianKetThucXacNhanNhapHoc',
      render: (_arr: QLDotTuyenSinh.Record[], obj: QLDotTuyenSinh.Record) => {
        return moment(obj?.thoiGianMoDangKy).format(`DD/MM/YYYY H:mm`);
      },
      align: 'center',
      width: 200,
    },
    {
      title: 'Số lượng đăng ký',
      dataIndex: 'soLuongDangKy',
      key: 'soLuongDangKy',
      width: 100,
      align: 'center',
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      fixed: 'right',
      width: 170,
      align: 'center',
      render: (obj: any, record: QLDotTuyenSinh.Record) => {
        return (
          <>
            <Button
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                setVisibleForm(true);
                setInitialValue(record);
                setEdit(true);
              }}
            />
            <Divider type="vertical" />
            <Popconfirm
              title="Có chắc chắn xoá"
              onConfirm={() => {
                deleteModel(record?._id);
              }}
            >
              <Button shape="circle" icon={<DeleteOutlined />} type="primary" />
            </Popconfirm>
          </>
        );
      },
    },
  ];
  const tableProperties: TableProps.Props = {
    modelName: 'qldottuyensinh',
    Form: MainForm,
    formType: 'Drawer',
    columns: columns,
    widthDrawer: '1000px',
    getData: (params: any) => {
      getModel(...params);
      getData(getAllCoSoDaoTao);
      getData(getAllHinhThucDaoTao);
      getData(getAllNamTuyenSinh);
      getData(getAllNganhChuyenNganh);
      getData(getAllDoiTuongTuyenSinh);
      getData(getPayMent);
    },
    dependencies: [limit, page, condition],
    loading: loading,
    params: [undefined, ''],
    hascreate: true,
  };
  return <TableBase {...tableProperties} />;
};

export default QuanLyDotTuyenSinh;
