import TableBase from '@/components/Table';
import type { QlChuyenNganh } from '@/services/QLChuyenNganh/typings';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Divider, Form, Input, Popconfirm, Space } from 'antd';
import React from 'react';
import { useModel } from 'umi';

const QuanLyChuyenNganhForm: React.FC = () => {
  const { postModel, edit, fieldData, putModel, setVisibleForm } = useModel('qlchuyennganh');
  return (
    <Form
      onFinish={(value) => {
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        edit ? putModel(fieldData?._id || 0, value) : postModel(value);
      }}
      layout="vertical"
      style={{
        margin: '2rem',
      }}
    >
      <div
        style={{
          padding: '1rem 0',
          marginBottom: '1rem',
          fontSize: '1rem',
          borderBottom: '1px solid whitesmoke',
        }}
      >
        {edit ? `Chỉnh sửa ngành/chuyên ngành` : `Thêm mới ngành/chuyên ngành`}
      </div>
      <Form.Item
        label="Tên ngành/chuyên ngành"
        name="ten"
        initialValue={edit ? fieldData?.ten : ''}
        rules={[{ required: true, message: 'Bắt buộc' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Mã ngành/chuyên ngành"
        name="ma"
        initialValue={edit ? fieldData?.ma : ''}
        rules={[{ required: true, message: 'Bắt buộc' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Ký hiệu ngành/chuyên ngành"
        name="kyHieu"
        initialValue={edit ? fieldData?.kyHieu : ''}
        rules={[{ required: true, message: 'Bắt buộc' }]}
      >
        <Input />
      </Form.Item>
      <Space
        size="small"
        style={{ display: 'flex', justifyContent: 'center', paddingBottom: '1rem' }}
      >
        <Button type="primary" htmlType="submit">
          Lưu
        </Button>
        <Button
          onClick={() => {
            setVisibleForm(false);
          }}
        >
          Đóng
        </Button>
      </Space>
    </Form>
  );
};

const QuanLyChuyenNganh: React.FC = () => {
  const {
    loading,
    limit,
    getModel,
    setEdit,
    setVisibleForm,
    setFieldData,
    deleteModel,
    page,
    condition,
  } = useModel('qlchuyennganh');
  const columns: any[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: 'Tên ngành',
      dataIndex: 'ten',
      key: 'ten',
    },
    {
      title: 'Mã ngành',
      dataIndex: 'ma',
      key: 'ma',
    },
    {
      title: 'Ký Hiệu',
      dataIndex: 'kyHieu',
      key: 'kyHieu',
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      render: (obj: QlChuyenNganh.Record) => {
        return (
          <>
            <Button
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                setEdit(true);
                setVisibleForm(true);
                setFieldData(obj);
              }}
            />
            <Divider type="vertical" />
            <Popconfirm
              title="Có chắc chắn xoá"
              onConfirm={() => {
                deleteModel(obj?._id);
              }}
            >
              <Button shape="circle" icon={<DeleteOutlined />} type="primary" />
            </Popconfirm>
          </>
        );
      },
    },
  ];
  const tableProperties = {
    modelName: 'qlchuyennganh',
    Form: QuanLyChuyenNganhForm,
    columns: columns,
    getData: (param: any) => {
      getModel(...param);
    },
    dependencies: [limit, page, condition],
    loading: loading,
    params: [undefined, ''],
    hascreate: true,
  };
  return <TableBase {...tableProperties} formType="Modal" />;
};

export default QuanLyChuyenNganh;
