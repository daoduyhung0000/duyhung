import { useCallback, useState } from "react"
import Decrement from "./component/decrement/decrement"
import Increment from "./component/increment/Increment"

const Caculator: React.FC = () => {
    const [result, setResult] = useState(0)
    const [soHang1, setSoHang1] = useState(0)
    const [soHang2, setSoHang2] = useState(0)
    const [view, setView] = useState<boolean>(false)
    const increment = useCallback(() => {
        setResult(soHang1+soHang2)
    }, [soHang1,soHang2])


    const increment1 =  ()=>  setResult(soHang1+soHang2);
    

    function decrement() {
        setResult(soHang1 - soHang2)
    }
    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div>Số hạng 1</div>
            <input type="number" onChange={(e) => setSoHang1(parseInt(e.target.value))} value={soHang1}
                style={{ width: '100px' }}
            />
            <div><Increment handlePlus={increment} /><Decrement decrement={decrement} /></div>
            <div>Số hạng 2</div>
            <input type="number" onChange={(e) => setSoHang2(parseInt(e.target.value))} value={soHang2}
                style={{ width: '100px' }}
            />
            <button onClick={()=>setView(!view)}>show/hide result</button>
            {view&& <div>{result}</div>}
            {/* <Result result={result} /> */}
        </div>
    )
}

export default Caculator