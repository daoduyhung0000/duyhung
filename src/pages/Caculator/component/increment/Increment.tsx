import { memo } from "react"

const Increment = (props: {handlePlus: Function})=>{
   
    console.log('+')
    return <button style={{width:'20px'}} onClick={()=>props.handlePlus()}>+</button>
}

export default memo(Increment)