
const Decrement: React.FC = (state)=>{
    return <button style={{width:'20px'}} onClick={()=> state.decrement()}>-</button>
}

export default Decrement