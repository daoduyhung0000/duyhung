import React from 'react';
import { useModel } from 'umi';
import TableBase from '@/components/Table';
import { QLPhuongThucXetTuyen } from '@/services/QLPhuongThucXetTuyen/typings';
import { Button, Divider, Form, Input, Popconfirm, Select, Space } from 'antd';
import { Option } from 'antd/lib/mentions';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';

const QuanLyPhuongThucXetTuyenForm: React.FC = () => {
  const { danhSachHinhThucDaoTao, postModel } = useModel('qlphuongthucxettuyen');

  return (
    <Form
      onFinish={(value) => {
        postModel(value);
      }}
      layout="vertical"
      style={{
        margin: '2rem',
      }}
    >
      <div
        style={{
          padding: '1rem 0',
          marginBottom: '1rem',
          fontSize: '1rem',
          borderBottom: '1px solid whitesmoke',
        }}
      >
        Thêm mới phương thức tuyển sinh
      </div>
      <Form.Item
        label="Hình thức đào tạo"
        name="hinhThucDaoTao"
        rules={[{ required: true, message: 'Bắt buộc' }]}
      >
        <Select>
          {danhSachHinhThucDaoTao.map((obj: QLPhuongThucXetTuyen.HinhThucDaoTao) => {
            return <Option value={obj.ten}>{obj.ten}</Option>;
          })}
        </Select>
      </Form.Item>
      <Form.Item
        label="Mã phương thức"
        name="maPhuongThuc"
        rules={[{ required: true, message: 'Bắt buộc' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Tên phương thức"
        name="tenPhuongThuc"
        rules={[{ required: true, message: 'Bắt buộc' }]}
      >
        <Input />
      </Form.Item>
      <Space
        size="small"
        style={{ display: 'flex', justifyContent: 'center', paddingBottom: '1rem' }}
      >
        <Button type="primary" htmlType="submit">
          Lưu
        </Button>
        <Button>Đóng</Button>
      </Space>
    </Form>
  );
};
const QuanLyPhuongThucXetTuyen: React.FC<object> = () => {
  const { loading, limit, getModel, getHinhThucDaoTao } = useModel('qlphuongthucxettuyen');
  const columns: any[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: 'Mã phương thức',
      dataIndex: 'maPhuongThuc',
      key: 'maPhuongThuc',
    },
    {
      title: 'Tên phương thức',
      dataIndex: 'tenPhuongThuc',
      key: 'tenPhuongThuc',
    },
    {
      title: 'Loại phương thức',
      dataIndex: 'loaiPhuongThuc',
      key: 'loaiPhuongThuc',
    },
    {
      title: 'Hình thức đào tạo',
      dataIndex: 'hinhThucDaoTao',
      key: 'hinhThucDaoTao',
      render: (params: QLPhuongThucXetTuyen.HinhThucDaoTao) => {
        return params.ten;
      },
    },
    {
      title: 'Thao tác',
      dataIndex: 'thaoTac',
      key: 'thaoTac',
      render: () => {
        return (
          <>
            <Button shape="circle" icon={<EditOutlined />} />
            <Divider type="vertical" />
            <Popconfirm title="Có chắc chắn xoá">
              <Button shape="circle" icon={<DeleteOutlined />} type="primary" />
            </Popconfirm>
          </>
        );
      },
    },
  ];
  const tableProperties = {
    modelName: 'qlphuongthucxettuyen',
    Form: QuanLyPhuongThucXetTuyenForm,
    columns: columns,
    getData: async (params: any) => {
      getModel(...params);
      getHinhThucDaoTao();
    },
    dependencies: [limit],
    loading: loading,
    params: [undefined, ''],
    hascreate: true,
  };
  return <TableBase {...tableProperties} formType="Modal" />;
};

export default QuanLyPhuongThucXetTuyen;
