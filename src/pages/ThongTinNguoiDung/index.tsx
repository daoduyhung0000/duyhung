import { ThongTinCaNhanTitle } from '@/utils/constants';
import {
  UserOutlined,
  MailOutlined,
  CalendarOutlined,
  AimOutlined,
  PhoneOutlined,
  SafetyCertificateOutlined,
} from '@ant-design/icons';
import { Layout, Avatar } from 'antd';
import { Content, Header } from 'antd/lib/layout/layout';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useModel } from 'umi';

const ThongTinNguoiDung = () => {
  const noData = 'Unknow';
  const [iconSize, iconMargin] = ['3rem', '1rem'];
  const { getThongTinCaNhanModel, user } = useModel('thongtincanhan');
  const [firstHeading, setFirstHeading] = useState('');
  const [secondHeading, setSecondHeading] = useState('');
  useEffect(() => {
    getThongTinCaNhanModel();
    setFirstHeading(ThongTinCaNhanTitle.PROFILE);
  }, []);

  useEffect(() => {
    setSecondHeading(
      `${user?.name?.title ?? ''} ${user?.name?.first ?? ''} ${user?.name?.last ?? ''}`,
    );
  }, [user]);

  function getDataDetail(type: string) {
    switch (type) {
      case 'NAME':
        setFirstHeading(ThongTinCaNhanTitle.PROFILE);
        if (user?.name?.title && user?.name?.first && user?.name?.last) {
          setSecondHeading(`${user.name.title} ${user.name.first} ${user.name.last}`);
        } else {
          setSecondHeading(noData);
        }
        break;
      case 'MAIL':
        setFirstHeading(ThongTinCaNhanTitle.EMAIL);
        setSecondHeading(user?.email ?? noData);
        break;
      case 'BIRTH':
        setFirstHeading(ThongTinCaNhanTitle.BIRTH);
        setSecondHeading(moment(user?.dob?.date).format('DD/MM/YYYY') ?? noData);
        break;
      case 'ADDRESS':
        setFirstHeading(ThongTinCaNhanTitle.ADDRESS);
        if (user?.location?.street?.number && user?.location?.street?.name) {
          setSecondHeading(`${user.location.street.number} ${user.location.street.name}`);
        } else {
          setSecondHeading(noData);
        }
        break;
      case 'PHONE':
        setFirstHeading(ThongTinCaNhanTitle.PHONE);
        setSecondHeading(user?.phone ?? noData);
        break;
      case 'PASSWORD':
        setFirstHeading(ThongTinCaNhanTitle.PASSWORD);
        setSecondHeading(user?.login?.password ?? noData);
        break;
    }
  }
  return (
    <Layout
      style={{
        borderRadius: '10px',
        overflow: 'hidden',
        boxShadow: '0 0 4px black',
        backgroundColor: 'white',
      }}
    >
      <Header
        style={{
          height: '10rem',
          backgroundColor: 'whitesmoke',
          borderBottom: '1px solid black',
          position: 'relative',
        }}
      >
        <Avatar
          style={{
            position: 'absolute',
            left: '50%',
            top: '100%',
            transform: 'translate(-50%, -50%)',
            outline: '2px solid black',
          }}
          size={200}
          src={user?.picture?.large}
        ></Avatar>
      </Header>
      <Content>
        <h2
          style={{
            paddingTop: '10rem',
            textAlign: 'center',
          }}
        >
          {firstHeading}
        </h2>
        <h1
          style={{
            padding: '0.1rem',
            textAlign: 'center',
            fontSize: '2rem',
          }}
        >
          {secondHeading}
        </h1>
        <ul
          style={{
            display: 'flex',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            padding: '0',
          }}
        >
          <li>
            <UserOutlined
              style={{
                fontSize: iconSize,
                margin: iconMargin,
              }}
              onClick={() => getDataDetail('NAME')}
            />
          </li>
          <li>
            <MailOutlined
              style={{
                fontSize: iconSize,
                margin: iconMargin,
              }}
              onClick={() => getDataDetail('MAIL')}
            />
          </li>
          <li>
            <CalendarOutlined
              style={{
                fontSize: iconSize,
                margin: iconMargin,
              }}
              onClick={() => getDataDetail('BIRTH')}
            />
          </li>
          <li>
            <AimOutlined
              style={{
                fontSize: iconSize,
                margin: iconMargin,
              }}
              onClick={() => getDataDetail('ADDRESS')}
            />
          </li>
          <li>
            <PhoneOutlined
              style={{
                fontSize: iconSize,
                margin: iconMargin,
              }}
              onClick={() => getDataDetail('PHONE')}
            />
          </li>
          <li>
            <SafetyCertificateOutlined
              style={{
                fontSize: iconSize,
                margin: iconMargin,
              }}
              onClick={() => getDataDetail('PASSWORD')}
            />
          </li>
        </ul>
      </Content>
    </Layout>
  );
};

export default ThongTinNguoiDung;
