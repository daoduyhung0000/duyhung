import _ from "lodash"
import { Select } from "antd"
import styled from "styled-components"
import { useModel } from "umi"
import { useCallback } from "react"

const CustomSelect = styled(Select)`
    width: 22rem
`
const SearchApi = () => {
    const { Option } = Select
    const { getDataByName , record } = useModel('searchApi')
    const handleSearch = (e: string) => {
        getDataByName(e)
    }
    function showDanhSach() {
        return record.map((data: {name: string}) => {
            return <Option value={data.name}>{data.name}</Option>
        })
    }
    const getData = useCallback(handleSearch, [])
    return (
        <CustomSelect 
            showSearch 
            placeholder="Some name"
            onSearch={_.debounce(getData, 700)}
            notFoundContent={null}
            defaultActiveFirstOption={false}
            showArrow={false}
            filterOption={false}
        >
        {showDanhSach()}
        </CustomSelect>
    )
}

export default SearchApi