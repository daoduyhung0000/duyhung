import rules from "@/utils/rules"
import { Button, Col, Form, Input, InputNumber, Row } from "antd"
import {Content as ContentAntd} from "antd/lib/layout/layout"
import styled from "styled-components"

const Content: React.FC = () => {
    const CustomContent = styled(ContentAntd)`
        background-color: #575757;
        border-radius: 5px;
    `
    const CustomForm = styled(Form)`
        margin: 1rem;
    `
    const CustomItem = styled(CustomForm.Item)`
        label{
            color: white;
            font-size: '18px';
            font-weight: 700;
        }
    `
    const CustomInputNumber = styled(InputNumber)`
        width: 100%;
        border-radius: 5px;
    `

    const CustomInput = styled(Input)`
        border-radius: 5px ;
    `
    const CustomCol = styled(Col)`
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    `
    const Total = styled.div`
        background-color: white;
        font-style: italic;
        font-weight: 700;
        padding: 0.5rem;
        border-radius: 5px;
    `
    return (
        <CustomContent>
            <CustomForm layout="vertical">
                <Row gutter={16}>
                    <Col span={12}>
                        <CustomItem
                            name={`firstName`}
                            label={`First name:`}
                            rules={[...rules.required]}
                        >
                            <CustomInput/>
                        </CustomItem>
                    </Col>
                    <Col span={12}>
                        <CustomItem
                            name={`lastName`}
                            label={`Last name:`}
                            rules={[...rules.required]}
                        >
                            <CustomInput/>
                        </CustomItem>
                    </Col>
                    <Col span={12}>
                        <CustomItem
                            name={`age`}
                            label={`Age:`}
                            rules={[...rules.required]}
                        >
                            <CustomInputNumber 
                                min={0} 
                                max={120}
                            />
                        </CustomItem>
                    </Col>
                    <Col span={12}>
                        <CustomItem
                            name={`address`}
                            label={`Address:`}
                            rules={[...rules.required]}
                        >
                            <CustomInput/>
                        </CustomItem>
                    </Col>
                    <CustomCol span={24}>
                        <Total>Total:</Total>
                        <div>
                            <Button>Save</Button>
                            <Button>Clear all data</Button>
                        </div> 
                    </CustomCol>
                </Row>
            </CustomForm>
        </CustomContent>
    )
}

export default Content