import { Layout } from "antd"
import styled from "styled-components"
import Content from "./StylesComponent/Content/Content"


const KhaiBaoThongTin: React.FC = () => {
    const CustomLayout = styled(Layout)`
        margin: 1rem;
    `
    return (
        <CustomLayout>
            <Content>123</Content>
        </CustomLayout>
    )
}

export default KhaiBaoThongTin