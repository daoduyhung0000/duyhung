import TableBase from '@/components/Table';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Divider, Popconfirm } from 'antd';
import React from 'react';
import { useModel } from 'umi';
import FormModel from './form';

const CoSoDaoTao: React.FC<any> = () => {
  const {
    getModel,
    page,
    limit,
    setEdit,
    setVisibleForm,
    setId,
    deleteModel,
    setFieldId,
    loading,
  } = useModel('cosodaotao');

  const columns: any[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
    },
    {
      title: 'Tên cơ sở',
      dataIndex: 'ten',
      key: 'ten',
      align: 'center',
    },
    {
      title: 'Tên viết tắt',
      dataIndex: 'tenVietTat',
      key: ' tenVietTat',
      align: 'center',
    },
    {
      title: 'Ký hiệu',
      dataIndex: 'kyHieu',
      key: 'kyHieu',
      align: 'center',
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      render: (id: any, obj: any) => {
        return (
          <>
            <Button
              shape="circle"
              onClick={() => {
                setEdit(true);
                setVisibleForm(true);
                setId(id);
                setFieldId(obj);
              }}
            >
              <EditOutlined />
            </Button>
            <Divider type="vertical"></Divider>
            <Popconfirm
              title="Có chắc chắn xoá"
              onConfirm={() => {
                deleteModel(id);
              }}
            >
              <Button shape="circle" type="primary">
                <DeleteOutlined />
              </Button>
            </Popconfirm>
          </>
        );
      },
      align: 'center',
      fixed: 'right',
      dataIndex: '_id',
    },
  ];

  const props = {
    modelName: 'cosodaotao',
    Form: FormModel,
    getData: (params: any[]) => {
      getModel(...params);
    },

    loading: loading,
    params: [undefined, ''],
    hascreate: true,
    dependencies: [page, limit],
  };
  return <TableBase {...props} formType="Modal" columns={columns} />;
};
export default CoSoDaoTao;
