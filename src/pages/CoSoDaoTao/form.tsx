import { Button, Col, Form, Input, Row, Space } from 'antd';
import { useModel } from 'umi';

const FormModel = () => {
  const [form] = Form.useForm();
  const { setVisibleForm, edit, fieldId, putModel, postModel, id } = useModel('cosodaotao');
  return (
    <div
      style={{
        padding: '1rem',
      }}
    >
      <Row
        style={{
          fontSize: '1rem',
          borderBottom: '1px solid whitesmoke',
        }}
      >
        <Col span={24}>{edit ? 'Chỉnh sửa cơ sở đào tạo' : 'Thêm mới cơ sở đào tạo'}</Col>
      </Row>
      <Row
        style={{
          margin: '2rem 0',
        }}
      >
        <Form
          onFinish={(values) => {
            if (edit) {
              putModel(id, values);
            } else {
              postModel(values);
            }
          }}
          form={form}
          layout="vertical"
          style={{
            width: '100%',
          }}
        >
          <Form.Item
            initialValue={edit ? fieldId?.ten : ''}
            label="Tên cơ sở"
            rules={[{ required: true, message: 'Bắt buộc' }]}
            name="ten"
          >
            <Input placeholder="Tên cơ sở"></Input>
          </Form.Item>
          <Form.Item
            initialValue={edit ? fieldId?.tenVietTat : ''}
            label="Tên viết tắt"
            rules={[{ required: true, message: 'Bắt buộc' }]}
            name="tenVietTat"
          >
            <Input placeholder="Tên viết tắt"></Input>
          </Form.Item>
          <Form.Item initialValue={edit ? fieldId?.kyHieu : ''} label="Ký hiệu" name="kyHieu">
            <Input placeholder="Ký hiệu"></Input>
          </Form.Item>
          <div
            style={{
              display: 'flex',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Space size="small">
              <Button type="primary" htmlType="submit">
                Lưu
              </Button>
              <Button
                onClick={() => {
                  setVisibleForm(false);
                }}
              >
                Đóng
              </Button>
            </Space>
          </div>
        </Form>
      </Row>
    </div>
  );
};

export default FormModel;
