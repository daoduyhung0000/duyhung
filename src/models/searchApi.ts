import { getByName } from "@/services/SearchApi/searchApi"
import { useState } from "react"

export default () => {
    const [record, setRecord] = useState([])
    
    async function getDataByName(name: string){
        const respone = await getByName(name)
        setRecord(respone?.data ?? [])
    }

    return {
        record,
        setRecord,
        getDataByName
    }
} 