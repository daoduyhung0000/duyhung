import useInitModel from '@/hooks/useInitModel';
import {
  getAllCoSoDaoTao,
  getAllDoiTuongTuyenSinh,
  getAllHinhThucDaoTao,
  getAllNamTuyenSinh,
  getAllNganhChuyenNganh,
  getPayMent,
} from '@/services/QLDotTuyenSinh/qldottuyensinh';
import { useState } from 'react';

export default () => {
  const [danhSach, setDanhSach] = useState<QLDotTuyenSinh.Record[]>([]);
  const [record, setRecord] = useState<QLDotTuyenSinh.Record>();
  const [coSoDaoTaoDanhSach, setCoSoDaoTaoDanhSach] = useState<CosoDaoTao.Record[]>([]);
  const [hinhThucDaoTaoDanhSach, setHinhThucDaoTaoDanhSach] = useState<HinhThucDaoTao.Record[]>([]);
  const [namTuyenSinhDanhSach, setNamTuyenSinhDanhSach] = useState<NamTuyenSinh.Record[]>([]);
  const [nganhChuyenNganhDanhSach, setNganhChuyenNganhDanhSach] = useState<
    NganhChuyenNganh.Record[]
  >([]);
  const [doiTuongTuyenSinhDanhSach, setDoiTuongTuyenSinhDanhSach] = useState<
    DoiTuongTuyenSinh.Record[]
  >([]);
  const [payMentDanhSach, setPayMentDanhSach] = useState<any[]>([]);
  const [hinhThucDaoTaoValue, setHinhThucDaoTaoValue] = useState<string>();
  const [namValue, setNamValue] = useState<string>('');
  const [phuongThucTuyenSinhValue, setPhuongThucTuyenSinhValue] = useState<any[]>([]);
  const [danhSachDoiTuong, setDanhSachDoiTuong] = useState<
    QLDotTuyenSinh.DanhSachDoiTuongTuyenSinh[]
  >([]);
  const [danhSachNganhTuyenSinh, setDanhSachNganhTuyenSinh] = useState<
    QLDotTuyenSinh.DanhSachNganhTuyenSinh[]
  >([]);
  const [danhSachGiayToNopHoSo, setDanhSachGiayToNopHoSo] = useState<
    QLDotTuyenSinh.ThongTinGiayToNopHoSo[]
  >([]);
  const [danhSachGiayToNopHoSoOnline, setDanhSachGiayToNopHoSoOnline] = useState<
    QLDotTuyenSinh.ThongTinGiayToNopHoSo[]
  >([]);
  const [danhSachGiayToXacNhan, setDanhSachGiayToXacNhan] = useState<
    QLDotTuyenSinh.DanhSachGiayToXacNhanNhapHoc[]
  >([]);
  const [danhSachThongTinKhaiXacNhan, setDanhSachThongTinKhaiXacNhan] = useState<
    QLDotTuyenSinh.DanhSachThongTinKhaiXacNhan[]
  >([]);
  const [danhSachHuongDanSuDung, setDanhSachHuongDanSuDung] = useState<
    QLDotTuyenSinh.DanhSachHuongDanSuDung[]
  >([]);
  const [initialValue, setInitialValue] = useState<QLDotTuyenSinh.Record>({});
  const { setLoading, ...properties } = useInitModel('dot-tuyen-sinh', '', setDanhSach, setRecord);
  async function getData(callback: any) {
    setLoading(true);
    const respone = await callback();
    switch (callback) {
      case getAllCoSoDaoTao:
        setCoSoDaoTaoDanhSach(respone?.data?.data ?? []);
        break;
      case getAllHinhThucDaoTao:
        setHinhThucDaoTaoDanhSach(respone?.data?.data ?? []);
        break;
      case getAllNamTuyenSinh:
        setNamTuyenSinhDanhSach(respone?.data?.data ?? []);
        break;
      case getAllNganhChuyenNganh:
        setNganhChuyenNganhDanhSach(respone?.data?.data ?? []);
        break;
      case getAllDoiTuongTuyenSinh:
        setDoiTuongTuyenSinhDanhSach(respone?.data?.data ?? []);
        break;
      case getPayMent:
        setPayMentDanhSach(respone?.data?.data?.result ?? []);
        break;
        defualt: break;
    }
    setLoading(false);
  }
  return {
    ...properties,
    danhSach,
    setRecord,
    record,
    getData,
    coSoDaoTaoDanhSach,
    setCoSoDaoTaoDanhSach,
    hinhThucDaoTaoDanhSach,
    setHinhThucDaoTaoDanhSach,
    namTuyenSinhDanhSach,
    setNamTuyenSinhDanhSach,
    nganhChuyenNganhDanhSach,
    doiTuongTuyenSinhDanhSach,
    setDoiTuongTuyenSinhDanhSach,
    setNganhChuyenNganhDanhSach,
    payMentDanhSach,
    setPayMentDanhSach,
    hinhThucDaoTaoValue,
    setHinhThucDaoTaoValue,
    namValue,
    setNamValue,
    phuongThucTuyenSinhValue,
    setPhuongThucTuyenSinhValue,
    danhSachDoiTuong,
    setDanhSachDoiTuong,
    danhSachNganhTuyenSinh,
    setDanhSachNganhTuyenSinh,
    danhSachGiayToNopHoSo,
    setDanhSachGiayToNopHoSo,
    danhSachGiayToNopHoSoOnline,
    setDanhSachGiayToNopHoSoOnline,
    danhSachGiayToXacNhan,
    setDanhSachGiayToXacNhan,
    danhSachThongTinKhaiXacNhan,
    setDanhSachThongTinKhaiXacNhan,
    danhSachHuongDanSuDung,
    setDanhSachHuongDanSuDung,
    initialValue,
    setInitialValue,
  };
};
