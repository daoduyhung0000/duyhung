import useInitModel from '@/hooks/useInitModel';
import { useState } from 'react';

export default () => {
  const [danhSach, setDanhSach] = useState([]);
  const [record, setRecord] = useState<QuanLyCoSoDaoTao.Record>();
  const [edit, setEdit] = useState(false);
  const [id, setId] = useState(0);
  const [fieldId, setFieldId] = useState<QuanLyCoSoDaoTao.Record>();
  const { getModel, postModel, ...properties } = useInitModel(
    'co-so-dao-tao',
    '',
    setDanhSach,
    setRecord,
  );
  return {
    ...properties,
    danhSach,
    record,
    setDanhSach,
    setRecord,
    getModel,
    postModel,
    edit,
    setEdit,
    id,
    setId,
    fieldId,
    setFieldId,
  };
};
