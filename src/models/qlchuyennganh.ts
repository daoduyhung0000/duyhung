import useInitModel from '@/hooks/useInitModel';
import { QlChuyenNganh } from '@/services/QLChuyenNganh/typings';
import { useState } from 'react';

export default () => {
  const [danhSach, setDanhSach] = useState<QlChuyenNganh.Record[]>([]);
  const [record, setRecord] = useState<QlChuyenNganh.Record>();
  const [fieldData, setFieldData] = useState<QlChuyenNganh.Record>();
  const [edit, setEdit] = useState<boolean>(false);
  const { ...properties } = useInitModel('nganh-chuyen-nganh', '', setDanhSach, setRecord);

  return {
    ...properties,
    danhSach,
    setDanhSach,
    record,
    setRecord,
    edit,
    setEdit,
    fieldData,
    setFieldData,
  };
};
