import useInitModel from '@/hooks/useInitModel';
import { getAllHinhThucDaoTao } from '@/services/QLPhuongThucXetTuyen/phuongthucxettuyen';
import { QLPhuongThucXetTuyen } from '@/services/QLPhuongThucXetTuyen/typings';
import { useState } from 'react';

export default () => {
  const [danhSach, setDanhSach] = useState<QLPhuongThucXetTuyen.Record[]>([]);
  const [record, setRecord] = useState<QLPhuongThucXetTuyen.Record>();
  const [danhSachHinhThucDaoTao, setDanhSachHinhThucDaoTao] = useState<
    QLPhuongThucXetTuyen.HinhThucDaoTao[]
  >([]);
  const { setLoading, ...properties } = useInitModel(
    'phuong-thuc-tuyen-sinh',
    '',
    setDanhSach,
    setRecord,
  );

  async function getHinhThucDaoTao() {
    setLoading(true);
    const respone = await getAllHinhThucDaoTao();
    setDanhSachHinhThucDaoTao(respone?.data?.data ?? []);
    setLoading(false);
  }
  return {
    ...properties,
    danhSach,
    setDanhSach,
    record,
    setRecord,
    danhSachHinhThucDaoTao,
    setDanhSachHinhThucDaoTao,
    getHinhThucDaoTao,
    setLoading,
  };
};
