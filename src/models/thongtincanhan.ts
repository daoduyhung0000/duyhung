import { getThongTinCaNhan } from '@/services/ThongTinCaNhan/thongTinCaNhan';
import { ThongTinCaNhan } from '@/services/ThongTinCaNhan/typings';
import { useState } from 'react';

export default () => {
  const [user, setUser] = useState<ThongTinCaNhan.Coordinates>({});
  const getThongTinCaNhanModel = async () => {
    const response = await getThongTinCaNhan();
    setUser(response?.data?.results?.[0]);
  };

  return {
    getThongTinCaNhanModel,
    user,
  };
};
